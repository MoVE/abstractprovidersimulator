package localValueSimulator;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import org.ornet.cdm.NumericMetricState;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.provider.SDCProvider;

public class MonotonicEqualStepMetricValueSimulator extends AbstractApsNumericMetricValueSimulator {
	
	/**
	 * the value that shall be the first of the simulation
	 */
	private double startValue = 0;
	
	/**
	 * step width for incrementing/decrementing the value
	 */
	private BigDecimal stepWidth = BigDecimal.valueOf(0);
	
	/**
	 * indicates whether the value shall be incremented or decremented
	 */
	private boolean increment = true;
	
	private boolean initialRun = true;
	

	
	public MonotonicEqualStepMetricValueSimulator(SDCProvider sdcProvider, String metricHandle, int samplingPeriod,
			double minValue, double maxValue, int decimalPlaces, double startValue, double stepWidth,
			boolean increment) {
		super(sdcProvider, metricHandle, samplingPeriod, minValue, maxValue, decimalPlaces);
		this.startValue = startValue;
		this.stepWidth = BigDecimal.valueOf(stepWidth);
		this.increment = increment;

	}



	public void start() {

		scheduler.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				
				if (initialRun) {
					// set initial value
					NumericMetricState nms = SDCToolbox.findState(sdcProvider, objectHandle, NumericMetricState.class);
					nms.getMetricValue().setValue(BigDecimal.valueOf(startValue));
					sdcProvider.updateState(nms);
					
					initialRun = false;
				}
				else {

					//update SDC representation
					NumericMetricState nms = SDCToolbox.findState(sdcProvider, objectHandle, NumericMetricState.class);
					
					BigDecimal oldValue = nms.getMetricValue().getValue();
					BigDecimal newValue;
					
					if(increment) {
						newValue = oldValue.add(stepWidth);
					}
					else {
						newValue = oldValue.subtract(stepWidth);
					}
					
					
					
					nms.getMetricValue().setValue(newValue);
					sdcProvider.updateState(nms);
					
					//FIXME use dedicated logger
					System.err.println("[Value Simulation] Metric Handle: '"
							+ "" + nms.getDescriptorHandle() + "' New Value: "
									+ "" + nms.getMetricValue().getValue());
				}
			}

		}, samplingPeriod, samplingPeriod, TimeUnit.MILLISECONDS);
	}

}
