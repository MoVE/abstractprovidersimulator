package localValueSimulator;

import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.VmdDescriptor;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import sdclib.ApsSdcProvider;

public class BehaviorDescriptionReader {
	
	public static final String NODE_EQUAL_DIST = "aps:EqualDistributionMetricBehaviorDescription";
	public static final String NODE_EQUAL_STEP = "aps:MonotonicEqualStepMetricBehaviorDescription";
	public static final String NODE_ALCON_PRES_TOGGLE = "aps:ToggleAlertConditionPresenceBehaviorDescription";
	
	public static final String ATTR_SAMPLING_PERIOD = "SamplingPeriod";
	public static final String ATTR_MAX_VAL = "MaximumValue";
	public static final String ATTR_MIN_VAL = "MinimumValue";
	public static final String ATTR_START_VAL = "StartValue";
	public static final String ATTR_STEP_WIDTH = "StepWidth";
	public static final String ATTR_INCREMENT = "Increment";
	
	
	/**
	 * reference to the provider object -> used to modify values
	 */
	private ApsSdcProvider sdcProvider = null;
	
	
	
	public BehaviorDescriptionReader(ApsSdcProvider sdcProvider) {
		super();
		this.sdcProvider = sdcProvider;
	}



	public MdsDescriptor getBehaviorFromMdDescription( MdsDescriptor mdsDescriptor ) {
		
		double maxValue = 0, minValue = 0, startValue = 0, stepWidth = 0;
		int samplingPeriod = 0, decimalPlaces = 0;
		boolean increment = true;
		
		// have a look into the MDS for alerts
		if ( mdsDescriptor.getAlertSystem() != null ) {
			handleAlertSystems(sdcProvider, mdsDescriptor.getAlertSystem() );
		}
		
		//iterate over the whole tree and look for metrics
		for (VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd()) {
			
			// have a look into the VMD for alerts
			if ( vmdDescriptor.getAlertSystem() != null ) {
				handleAlertSystems(sdcProvider, vmdDescriptor.getAlertSystem() );
			}
			
			for (ChannelDescriptor channelDescriptor : vmdDescriptor.getChannel()) {
				for (AbstractMetricDescriptor abstractMetricDescriptor : channelDescriptor.getMetric()) {
					
					// we found a metric with an extension 
					if (abstractMetricDescriptor.getExtension() != null ) {
						
						for (int i = 0; i < abstractMetricDescriptor.getExtension().getAny().size(); i++) {
							var node = (Node)abstractMetricDescriptor.getExtension().getAny().get(i);
							String name = node.getNodeName();
							
							NamedNodeMap attributeMap = node.getAttributes();
							
							// look for common attributes
							if (node != null && name != null) {
								
								//FIXME read decimal places from description!
								
								var attrNode = attributeMap.getNamedItem(ATTR_SAMPLING_PERIOD);
								if (attrNode != null) {
									samplingPeriod = Integer.parseInt(attrNode.getNodeValue());
								}
								
								attrNode = attributeMap.getNamedItem(ATTR_MIN_VAL);
								if (attrNode != null) {
									minValue = Double.parseDouble(attrNode.getNodeValue());
								}
								
								attrNode = attributeMap.getNamedItem(ATTR_MAX_VAL);
								if (attrNode != null) {
									maxValue = Double.parseDouble(attrNode.getNodeValue());
								}
							}
							
							
							if ( name.equals(NODE_EQUAL_DIST) ) {
								
								EqualDistMetricValueSimulator simulator = new EqualDistMetricValueSimulator(sdcProvider, abstractMetricDescriptor.getHandle(), samplingPeriod, minValue, maxValue, decimalPlaces);
								
								sdcProvider.addMetricValueSimulator(simulator);
								//starting will be done later
								//simulator.start();
									
							}
							else if ( name.equals(NODE_EQUAL_STEP) ) {
								var attrNode = attributeMap.getNamedItem(ATTR_START_VAL);
								if (attrNode != null) {
									startValue = Double.parseDouble(attrNode.getNodeValue());
								}
								
								attrNode = attributeMap.getNamedItem(ATTR_STEP_WIDTH);
								if (attrNode != null) {
									stepWidth = Double.parseDouble(attrNode.getNodeValue());
								}
								
								attrNode = attributeMap.getNamedItem(ATTR_INCREMENT);
								if (attrNode != null) {
									increment = Boolean.parseBoolean(attrNode.getNodeValue());
								}
								
								MonotonicEqualStepMetricValueSimulator simulator = new MonotonicEqualStepMetricValueSimulator(sdcProvider, abstractMetricDescriptor.getHandle(), samplingPeriod, minValue, maxValue, decimalPlaces, startValue, stepWidth, increment);
								sdcProvider.addMetricValueSimulator(simulator);
							}
						}						
					}
				}
			}
		}
		
		
		return mdsDescriptor;
		
		
	}
	
	private ApsSdcProvider handleAlertSystems(ApsSdcProvider sdcProvider, AlertSystemDescriptor alSysDesc) {
		
		int samplingPeriod = 0;
		
		if ( alSysDesc.getAlertCondition() != null ) {
			for ( AlertConditionDescriptor alconDesc : alSysDesc.getAlertCondition() ) {
					
				if (alconDesc.getExtension() != null ) {
					
					for (int i = 0; i < alconDesc.getExtension().getAny().size(); i++) {
						var node = (Node)alconDesc.getExtension().getAny().get(i);
						String name = node.getNodeName();
						
						NamedNodeMap attributeMap = node.getAttributes();
						
						// look for common attributes
						if (node != null && name != null) {
							
							//FIXME read decimal places from description!
							
							var attrNode = attributeMap.getNamedItem(ATTR_SAMPLING_PERIOD);
							if (attrNode != null) {
								samplingPeriod = Integer.parseInt(attrNode.getNodeValue());
							}
						}
						
						if ( name.equals(NODE_ALCON_PRES_TOGGLE) ) {
							var attrNode = attributeMap.getNamedItem(ATTR_START_VAL);
							boolean alConPresStartValue = false;
							if (attrNode != null) {
								alConPresStartValue = Boolean.parseBoolean(attrNode.getNodeValue());
							}

							
							AlertConditionToggleSimulator simulator = new AlertConditionToggleSimulator(sdcProvider, alconDesc.getHandle(), samplingPeriod, alConPresStartValue);
							sdcProvider.addMetricValueSimulator(simulator);
						}
					}
				}
			}
		}
		
		return sdcProvider;
	}

}
