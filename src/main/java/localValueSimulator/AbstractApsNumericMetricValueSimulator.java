package localValueSimulator;

import org.ornet.sdclib.provider.SDCProvider;

public abstract class AbstractApsNumericMetricValueSimulator extends AbstractApsSimulator {

	
	/**
	 * minimum value to be generated
	 */
	protected double minValue = 0;
	
	/**
	 * maximum value to be generated
	 */
	protected double maxValue = 0;
	
	/**
	 * number of decimal places of the value to be generated
	 */
	protected int decimalPlaces = 0;
	
	public AbstractApsNumericMetricValueSimulator(SDCProvider sdcProvider, String metricHandle, int samplingPeriod, double minValue,
			double maxValue, int decimalPlaces) {
		super(sdcProvider, metricHandle, samplingPeriod);
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.decimalPlaces = decimalPlaces;
	}
	
	protected double round(double valueToBeRounded) {
		return round(valueToBeRounded, decimalPlaces);
	}
	
	private double round(double valueToBeRounded, int decimalPlaces) {
		int factor = 1 * (int)Math.pow(10, decimalPlaces);
		return Math.round(valueToBeRounded * factor)/factor;
	}
}
