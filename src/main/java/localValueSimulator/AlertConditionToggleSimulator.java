package localValueSimulator;

import java.util.concurrent.TimeUnit;

import org.ornet.cdm.AlertConditionState;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.provider.SDCProvider;

public class AlertConditionToggleSimulator extends AbstractApsSimulator {
	
	/**
	 * first alert condition presence that shall be simulated 
	 */
	private boolean initialAlertConditionPresence = true;
	
	private boolean initialRun = true;

	public AlertConditionToggleSimulator(SDCProvider sdcProvider, String metricHandle, int samplingPeriod, boolean initialAlertConditionPresence) {
		super(sdcProvider, metricHandle, samplingPeriod);
		this.initialAlertConditionPresence = initialAlertConditionPresence;
	}

	@Override
	public void start() {
		scheduler.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {

				
				if (initialRun) {
					// set initial value
					AlertConditionState acs = SDCToolbox.findState(sdcProvider, objectHandle, AlertConditionState.class);
					acs.setPresence(initialAlertConditionPresence);
					sdcProvider.updateState(acs);
					
					initialRun = false;
				}
				else {
					// toggle the alert condition presence
					AlertConditionState acs = SDCToolbox.findState(sdcProvider, objectHandle, AlertConditionState.class);
					acs.setPresence(!acs.isPresence()); //toggling
					sdcProvider.updateState(acs);
					
					//FIXME use dedicated logger
					System.err.println("[Value Simulation] Alert Condition Handle: '"
							+ "" + acs.getDescriptorHandle() + "' New Presence Value: "
									+ "" + acs.isPresence());
				}
			}

		}, samplingPeriod, samplingPeriod, TimeUnit.MILLISECONDS);
		
	}



}
