package localValueSimulator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import org.ornet.sdclib.provider.SDCProvider;

public abstract class AbstractApsSimulator {

	protected ScheduledExecutorService scheduler = Executors
			.newScheduledThreadPool(1);

	/**
	 * reference to the provider object -> used to modify values
	 */
	protected SDCProvider sdcProvider = null;
	
	/**
	 * handle of the metric that shall be simulated
	 */
	protected String objectHandle = null;
	
	/**
	 * period of sampling new values in [ms]
	 */
	protected int samplingPeriod = 1000;
	

	public AbstractApsSimulator(SDCProvider sdcProvider, String metricHandle, int samplingPeriod) {
		super();
		this.sdcProvider = sdcProvider;
		this.objectHandle = metricHandle;
		this.samplingPeriod = samplingPeriod;
	}

	public void stop( ) {
		scheduler.shutdown();
	}
	
	abstract public void start();



}
