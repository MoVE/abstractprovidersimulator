package localValueSimulator;

import java.math.BigDecimal;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.MetricCategory;
import org.ornet.cdm.NumericMetricDescriptor;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.provider.SDCProvider;

import application.Config;
import application.UserInterfaceHandler;

@Deprecated
public class ApsValueSimulator {

	private ScheduledExecutorService scheduler = Executors
			.newScheduledThreadPool(1);

	/**
	 * reference to the provider object -> used to modify values
	 */
	private SDCProvider sdcProvider = null;
	

	public ApsValueSimulator(SDCProvider sdcProvider) {
		this.sdcProvider = sdcProvider;
	}
	
	public void stop( ) {
		scheduler.shutdown();
	}

	public void start() {
		scheduler.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				// random value between 0 and 100
				double randomValue = ThreadLocalRandom.current()
						.nextInt(100, 130);

				// find all metrics
				for (MdsDescriptor mdsDescriptor : sdcProvider
						.getMDDescription().getMds()) {
					for (VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd()) {
						for (ChannelDescriptor channelDescriptor : vmdDescriptor
								.getChannel()) {
							for (AbstractMetricDescriptor abstractMetricDescriptor : channelDescriptor
									.getMetric()) {
								// and set a random value if it is a numeric
								// metric that is not a setting nor presetting
								if (abstractMetricDescriptor instanceof NumericMetricDescriptor
										&& !(abstractMetricDescriptor
												.getMetricCategory() == MetricCategory.SET || abstractMetricDescriptor
												.getMetricCategory() == MetricCategory.PRESET)) {


									//update SDC representation
									NumericMetricState nms = SDCToolbox.findState(sdcProvider, abstractMetricDescriptor.getHandle(), NumericMetricState.class);
									nms.getMetricValue().setValue(BigDecimal.valueOf(randomValue--));
									sdcProvider.updateState(nms);
									
									//FIXME use dedicated logger
									System.err.println("[Value Simulation] Metric Handle: '"
											+ "" + nms.getDescriptorHandle() + "' New Value: "
													+ "" + nms.getMetricValue().getValue());
									
									if(UserInterfaceHandler.active && nms.getDescriptorHandle().equals("m_bp_sys")){
										UserInterfaceHandler.ui.setText(nms.getMetricValue().getValue().toString());
									}
								}
							}
						}
					}
				}

			}

		}, Config.getInstance().getValueSimulationUpdatePeriod(), Config.getInstance().getValueSimulationUpdatePeriod(), TimeUnit.MILLISECONDS);
	}

}
