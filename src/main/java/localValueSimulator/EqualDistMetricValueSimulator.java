package localValueSimulator;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.ornet.cdm.NumericMetricState;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.provider.SDCProvider;

import logging.SdcLogger;

public class EqualDistMetricValueSimulator extends AbstractApsNumericMetricValueSimulator {

	public EqualDistMetricValueSimulator(SDCProvider sdcProvider, String metricHandle, int samplingPeriod,
			double minValue, double maxValue, int decimalPlaces) {
		super(sdcProvider, metricHandle, samplingPeriod, minValue, maxValue, decimalPlaces);

	}
	
	public void start() {

		scheduler.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {

				
				double randomValue = 0;
				
				try {
					randomValue = ThreadLocalRandom.current().nextDouble(minValue, maxValue);
				} catch (Exception e) {
					SdcLogger.log.severe("ERROR: Problem while generating a random value!");
				}
				
				
				randomValue = round(randomValue);

				//update SDC representation
				NumericMetricState nms = SDCToolbox.findState(sdcProvider, objectHandle, NumericMetricState.class);
				nms.getMetricValue().setValue(BigDecimal.valueOf(randomValue));
				sdcProvider.updateState(nms);
				
				//FIXME use dedicated logger
				System.err.println("[Value Simulation] Metric Handle: '"
						+ "" + nms.getDescriptorHandle() + "' New Value: "
								+ "" + nms.getMetricValue().getValue());

			}

		}, samplingPeriod, samplingPeriod, TimeUnit.MILLISECONDS);
	}

}
