package sdclib;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractDescriptor;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.AbstractMetricDescriptor.Relation;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractMetricValue;
import org.ornet.cdm.AbstractOperationDescriptor;
import org.ornet.cdm.AbstractOperationState;
import org.ornet.cdm.AbstractSetStateOperationDescriptor;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.ActivateOperationDescriptor;
import org.ornet.cdm.AlertActivation;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertConditionMonitoredLimits;
import org.ornet.cdm.AlertConditionState;
import org.ornet.cdm.AlertSignalDescriptor;
import org.ornet.cdm.AlertSignalPresence;
import org.ornet.cdm.AlertSignalState;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.ChannelState;
import org.ornet.cdm.CodedValue;
import org.ornet.cdm.ComponentActivation;
import org.ornet.cdm.ContextAssociation;
import org.ornet.cdm.EnsembleContextDescriptor;
import org.ornet.cdm.EnsembleContextState;
import org.ornet.cdm.EnumStringMetricDescriptor;
import org.ornet.cdm.EnumStringMetricDescriptor.AllowedValue;
import org.ornet.cdm.EnumStringMetricState;
import org.ornet.cdm.Mdib;
import org.ornet.cdm.InstanceIdentifier;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.LimitAlertConditionDescriptor;
import org.ornet.cdm.LimitAlertConditionState;
import org.ornet.cdm.LocationContextDescriptor;
import org.ornet.cdm.LocationContextState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.MdsState;
import org.ornet.cdm.MeansContextDescriptor;
import org.ornet.cdm.MeansContextState;
import org.ornet.cdm.MeasurementValidity;
import org.ornet.cdm.MetricAvailability;
import org.ornet.cdm.MetricCategory;
import org.ornet.cdm.NumericMetricDescriptor;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.NumericMetricValue;
import org.ornet.cdm.OperatingMode;
import org.ornet.cdm.OperatorContextDescriptor;
import org.ornet.cdm.OperatorContextState;
import org.ornet.cdm.PatientContextDescriptor;
import org.ornet.cdm.PatientContextState;
import org.ornet.cdm.Range;
import org.ornet.cdm.ScoDescriptor;
import org.ornet.cdm.SetStringOperationDescriptor;
import org.ornet.cdm.SetStringOperationState;
import org.ornet.cdm.SetValueOperationDescriptor;
import org.ornet.cdm.SetValueOperationState;
import org.ornet.cdm.StringMetricDescriptor;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.StringMetricValue;
import org.ornet.cdm.SystemContextDescriptor;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.cdm.VmdState;
import org.ornet.cdm.WorkflowContextDescriptor;
import org.ornet.cdm.WorkflowContextState;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.binding.mdpws.MDPWSTransportLayerConfiguration;
import org.ornet.sdclib.binding.mdpws.MDPWSTransportLayerDetail;
import org.ornet.sdclib.provider.OperationInvocationContext;
import org.ornet.sdclib.provider.SDCProviderActivateOperationHandler;
import org.ornet.sdclib.provider.SDCProviderAlertConditionStateHandler;
import org.ornet.sdclib.provider.SDCProviderMDStateHandler;


import application.Config;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.PemKeyCertOptions;
import io.vertx.core.net.PemTrustOptions;
import localValueSimulator.BehaviorDescriptionReader;
import sdclib.util.Util;
import util.Constants;
import logging.SdcLogger;
import mdibLoading.MdibFileReader;
import move.imd.MoveExtensionConfig;
import move.imd.messageManipulation.MessageManipulationConfig;
import util.TypeCodeCalculator;


/**
 * 
 * @author Martin Kasparick (IMD, University of Rostock)
 * @note Some parts of the code might go back to the examples provided by
 *       Andreas Besting (SurgiTAIX, Aachen)
 * @todo FIXME refactor type in class name (provicer -> provider) 
 */
public class ApsProvicerFactory {
	
	/**
	 * Prefix of the MoVE Backbone interface Operation Handles being generated for ALL metrics
	 */
	public static final String MOVE_BB_SCO_METRIC_PREFIX = "BB_SCO_";

	/**
	 * object, representing the SDC service provider
	 */
	private static ApsSdcProvider sdcProvider;
	
	
	public static void initMirroredProviders(
			ApsSdcProvider providerDut, ApsSdcProvider providerBackbone, 
			String providerDutMdDescription, 
			String providerDutEPR, 
			String providerBackboneEPR
			) {
		
		// make the providers available for the corresponding mirror
		providerDut.setMirrorSdcProvider(providerBackbone);
		providerBackbone.setMirrorSdcProvider(providerDut);
		
		// init provider for the DUT interface
		providerDut = ApsProvicerFactory.stateAndHandlerHandling(providerDut, providerDutMdDescription, false, providerBackbone);		
		MDPWSTransportLayerConfiguration transportLayerConfigurationDut = new MDPWSTransportLayerConfiguration();
		transportLayerConfigurationDut.getConfigurationDetail().setPortStart(Config.getInstance().getDutInterfaceStartPort());
		transportLayerConfigurationDut.getConfigurationDetail().setBindInterface(Config.getInstance().getDutInterfaceIp());
		
		if(Config.getInstance().isDutInterfaceUseTLS()) {			
			MDPWSTransportLayerDetail transportLayerDetailDut = transportLayerConfigurationDut.getConfigurationDetail();			
			enableTLS(transportLayerDetailDut);
		}
		
		providerDut.setTransportLayerConfiguration(transportLayerConfigurationDut);
		providerDut.setEndpointReference(providerDutEPR);
		
		// init provider for the Backbone interface
		providerBackbone = ApsProvicerFactory.stateAndHandlerHandling(providerBackbone, providerDutMdDescription, true, providerDut);		
		MDPWSTransportLayerConfiguration transportLayerConfigurationBB = new MDPWSTransportLayerConfiguration();
		transportLayerConfigurationBB.getConfigurationDetail().setPortStart(Config.getInstance().getBackboneInterfaceStartPort());
		MoveExtensionConfig.getInstance().setBackbonePort(Config.getInstance().getBackboneInterfaceStartPort() + 1);
		transportLayerConfigurationBB.getConfigurationDetail().setBindInterface(Config.getInstance().getBackboneInterfaceIp());
		
		if(Config.getInstance().isBackboneInterfaceUseTLS()) {			
			MDPWSTransportLayerDetail transportLayerDetailBB = transportLayerConfigurationBB.getConfigurationDetail();			
			enableTLS(transportLayerDetailBB);
		}
		
		providerBackbone.setTransportLayerConfiguration(transportLayerConfigurationBB);
		providerBackbone.setEndpointReference(providerBackboneEPR);
		
	}
	
	/**
	 * FIXME align with Toolbox
	 * @param transportLayerDetail
	 */
	private static void enableTLS(MDPWSTransportLayerDetail transportLayerDetail) {
				
		HttpClientOptions co = transportLayerDetail.getClientOptions();
		PemTrustOptions trustOptions = new PemTrustOptions();
		trustOptions.addCertPath("certs/cacert.pem");
		co.setTrustOptions(trustOptions);
		co.setVerifyHost(false);
		co.setSsl(true);
		co.setTrustAll(true);
		
		HttpServerOptions so = transportLayerDetail.getServerOptions();
		so.setSsl(true);
		so.addEnabledSecureTransportProtocol("TLSv1.3"); //enabling tls 1.3. looks like it is not enabled by default
		PemKeyCertOptions keyCertOptions = new PemKeyCertOptions();
		keyCertOptions.setCertPath("certs/sdccert.pem");
		keyCertOptions.setKeyPath("certs/userkey.pem");
		so.setPemKeyCertOptions(keyCertOptions);
	}
	

	public static ApsSdcProvider stateAndHandlerHandling(ApsSdcProvider providerToBeInited, String mdibContent, boolean isBackboneInterface, ApsSdcProvider _correspondingOtherSdcProvider) {
		
//		correspondingOtherSdcProvider = _correspondingOtherSdcProvider;
		
		// initialize the sdc provider object
		sdcProvider = providerToBeInited;

		// load MdDescription from MDIB file content
		MdDescription mdd = MdibFileReader.getMdDescriptionFromString(mdibContent);
		sdcProvider.setMDDescription(mdd);

		
//		// FIXME just for testing: create a metric and operation that is not part of the DUT interface
//		if(isBackboneInterface) {
//			StringMetricDescriptor smd = new StringMetricDescriptor();
//	        smd.setMetricCategory(MetricCategory.SET);
//	        smd.setMetricAvailability(MetricAvailability.CONT);
//	        CodedValue unit = new CodedValue();
//	        unit.setCodingSystem("11073");
//	        unit.setCode("MDC_DIMLESS");
//	        smd.setUnit(unit);
//	        CodedValue type = new CodedValue();
//	        type.setCodingSystem("11073");
//	        type.setCode("MDCX_MY_FOO_TYPE");
//	        smd.setType(type);
//	        smd.setHandle("foo");
//	        
//	        MdsDescriptor mds = sdcProvider.getMDDescription().getMds().get(0);
//			sdcProvider.removeMDS(mds.getHandle());
//			sdcProvider.addMDS(mds);
//			
//			mds.getVmd().get(0).getChannel().get(0).getMetric().add(smd);
//
//		}
		
		// FIXME this is a quick-and-very-dirty hack top get access to real MDS object and not only to a copy
		MdsDescriptor mds = sdcProvider.getMDDescription().getMds().get(0);
		sdcProvider.removeMDS(mds.getHandle());

		// remove extension from MDS
		mds.setExtension(null);
		
		sdcProvider.addMDS(mds);
		
//		sdcProvider = addSetOperationsForAllSetAndPresetMetrics(sdcProvider, mds);
		sdcProvider = addSetOperationsForAllEntriesInAllScos(sdcProvider, mds);
			
		if(isBackboneInterface) {
					
			// FIXME this is a quick-and-very-dirty hack top get access to real MDS object and not only to a copy
			mds = sdcProvider.getMDDescription().getMds().get(0);
			sdcProvider.removeMDS(mds.getHandle());
			sdcProvider.addMDS(mds);
			sdcProvider = addBackboneMessageManipulationConfig(sdcProvider, mds);

			// FIXME this is a quick-and-very-dirty hack top get access to real MDS object and not only to a copy
			mds = sdcProvider.getMDDescription().getMds().get(0);
			sdcProvider.removeMDS(mds.getHandle());
			sdcProvider.addMDS(mds);
			sdcProvider = addBackboneApsConfiguration(sdcProvider, mds);
			
			// FIXME this is a quick-and-very-dirty hack top get access to real MDS object and not only to a copy
			mds = sdcProvider.getMDDescription().getMds().get(0);
			sdcProvider.removeMDS(mds.getHandle());
			sdcProvider.addMDS(mds);
			
			sdcProvider = addBackboneSetOperations(sdcProvider, mds);
			sdcProvider = addBackboneConfigVmd(sdcProvider, "h_bb_vmd", "MDCX_VMD_BB");
			sdcProvider = addBackboneChannelToVmd(sdcProvider, "h_bb_act_opt_channel", "MDCX_CHANNEL_ACT_OP_BB", "h_bb_vmd");
			sdcProvider = addBackboneMetricsForActivateOperations(sdcProvider, "h_bb_act_opt_channel");
			
		}


		// now we have the device description
		// let's deal with the device state
		// for the metric state, we create a new instance of our own state
		// handler
		sdcProvider = addHandlerForAllMetrics(sdcProvider);
		
		// for all alerts, we crate a new instance of the state handler
		sdcProvider = addHandlerForAllAlertConditionsAndSignals(sdcProvider);
		
		// for all activate operations within the SCO, we create an instance of the state handler
		sdcProvider = addHandlerForAllActivateOperations(sdcProvider, isBackboneInterface);
		
		// for all contexts
		sdcProvider = addHandlerForAllContexts(sdcProvider);
		
		// for all operations
		// FIXME check up whether multiple states are created for on descriptor
		sdcProvider = addHandlerForAllOperations(sdcProvider);

		// for the component states (states of the nodes in the three top levels
		// (MDS, VMD and Channel) we use a very simple state handler only
		sdcProvider = addHandlerForAllComplexDeviceComponents(sdcProvider);
		
		// let's have a look whether there is a behavior description in one of the metric descriptor extension 
		if (!isBackboneInterface) {
			
			BehaviorDescriptionReader bdr = new BehaviorDescriptionReader(sdcProvider);
			
			mds = sdcProvider.getMDDescription().getMds().get(0);
			sdcProvider.removeMDS(mds.getHandle());
			
			mds= bdr.getBehaviorFromMdDescription(mds);
		
			sdcProvider.addMDS(mds);
		}
		
		
		return sdcProvider;

	}
	
	private static ApsSdcProvider addBackboneMessageManipulationConfig(ApsSdcProvider sdcProvider, MdsDescriptor mds) {
		
		// for message manipulation
		sdcProvider = addBackboneConfigVmd(sdcProvider, Constants.HANDLE_MSG_MANIPULATION_VMD, "MDCX_VMD_BB_MSG_MANIPULATION");
		sdcProvider = addBackboneChannelToVmd(sdcProvider, Constants.HANDLE_MSG_MANIPULATION_CHANNEL, "MDCX_CHANNEL_MSG_MANIPULATION_SINGLE_STATE_VERSION", Constants.HANDLE_MSG_MANIPULATION_VMD);

		StringMetricDescriptor smdDescrHandle = new StringMetricDescriptor();
		smdDescrHandle.setMetricCategory(MetricCategory.SET);
		smdDescrHandle.setMetricAvailability(MetricAvailability.CONT);
        CodedValue unit = new CodedValue();
        unit.setCodingSystem("11073");
        unit.setCode("MDC_DIMLESS");
        smdDescrHandle.setUnit(unit);
        CodedValue type = new CodedValue();
        type.setCodingSystem("11073");
        type.setCode("MDCX_MY_FOO_TYPE");
        smdDescrHandle.setType(type);
        smdDescrHandle.setHandle(Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_DESC_HANDLE);
        
        sdcProvider = addBackboneMetricToChannel(sdcProvider, Constants.HANDLE_MSG_MANIPULATION_CHANNEL, smdDescrHandle);
//        sdcProvider.createSetOperationForDescriptor(MOVE_BB_SCO_METRIC_PREFIX + smdDescrHandle.getHandle(), smdDescrHandle, mds);
        
		StringMetricDescriptor smdStateVersion = new StringMetricDescriptor();
		smdStateVersion.setMetricCategory(MetricCategory.SET);
		smdStateVersion.setMetricAvailability(MetricAvailability.CONT);
        smdStateVersion.setUnit(unit);
        smdStateVersion.setType(type);
        smdStateVersion.setHandle(Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_STATE_VERSION);
        
        sdcProvider = addBackboneMetricToChannel(sdcProvider, Constants.HANDLE_MSG_MANIPULATION_CHANNEL, smdStateVersion);
//        sdcProvider.createSetOperationForDescriptor(MOVE_BB_SCO_METRIC_PREFIX + smdStateVersion.getHandle(), smdStateVersion, mds);

        return sdcProvider;
		
	}
	
	private static ApsSdcProvider addBackboneApsConfiguration(ApsSdcProvider sdcProvider, MdsDescriptor mds) {
		
		// for message manipulation
		sdcProvider = addBackboneConfigVmd(sdcProvider, Constants.HANDLE_CONFIG_VMD, "MDCX_VMD_BB_CONFIG");
		sdcProvider = addBackboneChannelToVmd(sdcProvider, Constants.HANDLE_CONFIG_PROVIDER_STATE_CHANNEL, 
				"MDCX_CHANNEL_CONFIG_PROVIDER_STATE", Constants.HANDLE_CONFIG_VMD);

		EnumStringMetricDescriptor esmdDescrHandle = new EnumStringMetricDescriptor();
		esmdDescrHandle.setMetricCategory(MetricCategory.SET);
		esmdDescrHandle.setMetricAvailability(MetricAvailability.CONT);
		
        CodedValue unit = new CodedValue();
        unit.setCode(Util.calcDimensionTypeCode(Util.MDC_DIM_DIMLESS));
        unit.setSymbolicCodeName("MDC_DIM_DIMLESS");
        esmdDescrHandle.setUnit(unit);
        
        CodedValue type = new CodedValue();
        type.setCode(Util.calcTypeCodePrivateBlock(0xF042));
        type.setSymbolicCodeName("MDCX_DUT_PROVIDER_OPERATION_STATE");
        esmdDescrHandle.setType(type);
        esmdDescrHandle.setHandle(Constants.HANDLE_CONFIG_PROVIDER_OPERATION_STATE_METRIC);
        
        AllowedValue allowedValue = new AllowedValue();
        allowedValue.setValue(Constants.DUT_PROVIDER_OPERATION_STATE_RUNNING);
        esmdDescrHandle.getAllowedValue().add(allowedValue);
        allowedValue = new AllowedValue();
        allowedValue.setValue(Constants.DUT_PROVIDER_OPERATION_STATE_NOT_RUNNING);
        esmdDescrHandle.getAllowedValue().add(allowedValue);
        
        sdcProvider = addBackboneMetricToChannel(sdcProvider, Constants.HANDLE_CONFIG_PROVIDER_STATE_CHANNEL, esmdDescrHandle);


        return sdcProvider;
		
	}

	
	/**
	 * This method adds set operations to the SCO for all metrics with metric
	 * category equal to SET or PRESET.
	 * 
	 * FIXME transfer to toolbox
	 * 
	 * @param sdcProvider
	 * @param mds
	 * @return
	 */
	private static ApsSdcProvider addSetOperationsForAllSetAndPresetMetrics(ApsSdcProvider sdcProvider, MdsDescriptor mds) {
		for (MdsDescriptor mdsDescriptor : sdcProvider.getMDDescription().getMds()) {
			for (VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd()) {
				for (ChannelDescriptor channelDescriptor : vmdDescriptor.getChannel()) {
					for (AbstractMetricDescriptor abstractMetricDescriptor : channelDescriptor.getMetric()) {
						if (abstractMetricDescriptor.getMetricCategory() == MetricCategory.SET
								|| abstractMetricDescriptor.getMetricCategory() == MetricCategory.PRESET) {
							sdcProvider.createSetOperationForDescriptor(abstractMetricDescriptor, mds);
						}
					}
				}
			}
		}
		return sdcProvider;
	}
	
	private static ApsSdcProvider addSetOperationsForAllEntriesInAllScos(ApsSdcProvider sdcProvider, MdsDescriptor mds) {
		for (MdsDescriptor mdsDescriptor : sdcProvider.getMDDescription().getMds()) {
			
			if ( mdsDescriptor.getSco() != null ) {
				ScoDescriptor scoDesc = mdsDescriptor.getSco();
				sdcProvider = addSetOperationsForAllEntriesInSco(sdcProvider, mds, scoDesc);
			}
			
			for ( VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd() ) {
				if ( vmdDescriptor.getSco() != null ) {
					ScoDescriptor scoDesc = vmdDescriptor.getSco();
					sdcProvider = addSetOperationsForAllEntriesInSco(sdcProvider, mds, scoDesc);
				}
			}
		}
		
		return sdcProvider;
	}
	
	private static ApsSdcProvider addSetOperationsForAllEntriesInSco(ApsSdcProvider sdcProvider, MdsDescriptor mds, ScoDescriptor scoDesc) {
		AbstractDescriptor abstractDescriptor = null;

		for ( AbstractOperationDescriptor aod : scoDesc.getOperation() ) {
			
			if ( aod instanceof SetStringOperationDescriptor || aod instanceof SetValueOperationDescriptor ) {
				abstractDescriptor = SDCToolbox.findMetricDescriptor(sdcProvider, aod.getOperationTarget());
			}
			else if ( aod instanceof AbstractSetStateOperationDescriptor) {
				abstractDescriptor = SDCToolbox.findContextDescritor(sdcProvider, aod.getOperationTarget());
			}
			
			if (abstractDescriptor != null) {
				SdcLogger.log.info("Creating Operations for SCO Entry: Handle: " + aod.getHandle() + " Target: " + aod.getOperationTarget() + ""
						+ " MDS-Handle: " + mds.getHandle() + " isBackbone: " + sdcProvider.isBackboneProvider() + " MDS: " + mds.toString());
				sdcProvider.createSetOperationForDescriptor(aod.getHandle(), abstractDescriptor, mds);
			}
			else {
				SdcLogger.log.severe("Problem creating Operations for SCO Entries! (Handle: " + aod.getHandle() + " Target: " + aod.getOperationTarget() + ")");
			}
		}
		
		return sdcProvider;
	}
	
	
	/**
	 * TODO ask Andreas whether it is useful to do it this way.
	 * FIXME transfer to Toolbox, as it is used here and in other project (e.g. pump)
	 * @param sdcProvider
	 * @return
	 */
	private static ApsSdcProvider addHandlerForAllOperations(ApsSdcProvider sdcProvider) {

		for (MdsDescriptor nextMds : sdcProvider.getMDDescription().getMds()) {
			if (nextMds.getSco() != null) {
				ScoDescriptor sco = nextMds.getSco();
				for (AbstractOperationDescriptor aod : sco.getOperation()) {
					// we already did this for Activate Operations
					if (!(aod instanceof ActivateOperationDescriptor)) { //FIXME It looks like we don't... :-(
						
						if( aod instanceof SetValueOperationDescriptor ) {
							sdcProvider.addHandler(new SetValueOperationStateHandler(aod.getHandle()));
						}
						else if( aod instanceof SetStringOperationDescriptor ) {
							sdcProvider.addHandler(new SetStringOperationStateHandler(aod.getHandle()));
						}
					}
				}
			}
		}
		return sdcProvider;
	}
	
	/**
	 * TODO
	 * FIXME transfer to Toolbox, as it is used here and in other project (e.g. pump)
	 * 
	 *
	 */
	private static class SetValueOperationStateHandler extends SDCProviderMDStateHandler<SetValueOperationState> {

		String descriptorHandle = null;

		public SetValueOperationStateHandler(String descriptorHandle) {
			super(descriptorHandle);
			this.descriptorHandle = descriptorHandle;
		}

		@Override
		protected SetValueOperationState getInitialState() {
			SetValueOperationState svos = new SetValueOperationState();
			svos.setDescriptorHandle(descriptorHandle);
			svos.setOperatingMode(OperatingMode.EN);

			return svos;
		}
	}
	
	/**
	 * TODO
	 * FIXME transfer to Toolbox, as it is used here and in other project (e.g. pump)
	 * 
	 *
	 */
	private static class SetStringOperationStateHandler extends SDCProviderMDStateHandler<SetStringOperationState> {

		String descriptorHandle = null;

		public SetStringOperationStateHandler(String descriptorHandle) {
			super(descriptorHandle);
			this.descriptorHandle = descriptorHandle;
		}

		@Override
		protected SetStringOperationState getInitialState() {
			SetStringOperationState ssos = new SetStringOperationState();
			ssos.setDescriptorHandle(descriptorHandle);
			ssos.setOperatingMode(OperatingMode.EN);

			return ssos;
		}
	}
	
		
	/**
	 * This method adds set operations to the SCO for ALL metrics. 
	 * This will be used for the MoVE Backbone interface
	 * 
	 * @param sdcProvider
	 * @param mds
	 * @return
	 */
	private static ApsSdcProvider addBackboneSetOperations(ApsSdcProvider sdcProvider, MdsDescriptor mds) {
		for (MdsDescriptor mdsDescriptor : sdcProvider.getMDDescription().getMds()) {
			for (VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd()) {
				for (ChannelDescriptor channelDescriptor : vmdDescriptor.getChannel()) {
					for (AbstractMetricDescriptor abstractMetricDescriptor : channelDescriptor.getMetric()) {
						sdcProvider.createSetOperationForDescriptor(MOVE_BB_SCO_METRIC_PREFIX + abstractMetricDescriptor.getHandle(), abstractMetricDescriptor, mds);
					}
				}
			}
		}
		return sdcProvider;
	}
	
	/**
	 * 
	 * @param sdcProvider
	 * @param mds
	 * @return
	 */
	private static ApsSdcProvider addBackboneConfigVmd(ApsSdcProvider sdcProvider, String vmdHandle, String vmdCode) {
		MdsDescriptor mdsDescriptor = sdcProvider.getMDDescription().getMds().get(0); 
		
		sdcProvider.removeMDS(mdsDescriptor.getHandle()); // remove the "old" MDS
				
		// create VMD
		VmdDescriptor bbVmd = new VmdDescriptor();
		bbVmd.setHandle(vmdHandle);
		CodedValue bbVmdType = new CodedValue();
		bbVmdType.setCode(vmdCode);
		bbVmd.setType(bbVmdType);
			
		// add VMD to MDS
		mdsDescriptor.getVmd().add(bbVmd);
		
		// add MDS to Provider
		sdcProvider.addMDS(mdsDescriptor);
		
		return sdcProvider;
	}
	
	private static ApsSdcProvider addBackboneChannelToVmd(ApsSdcProvider sdcProvider, String channelHandle, String channelCode, String vmdHandle) {
		MdsDescriptor mdsDescriptor = sdcProvider.getMDDescription().getMds().get(0); 
		
		sdcProvider.removeMDS(mdsDescriptor.getHandle()); // remove the "old" MDS
				
		// search for VMD
		for ( VmdDescriptor vmdDescr : mdsDescriptor.getVmd() ) {
			if ( vmdDescr.getHandle().equals(vmdHandle) ) {
				// create Channel
				ChannelDescriptor bbActOpChannel = new ChannelDescriptor();
				bbActOpChannel.setHandle(channelHandle);
				CodedValue bbChannelType = new CodedValue();
				bbChannelType.setCode(channelCode);
				bbActOpChannel.setType(bbChannelType);
				
				// add Channel to VMD
				vmdDescr.getChannel().add(bbActOpChannel);
			}
		}
		
		// add MDS to Provider
		sdcProvider.addMDS(mdsDescriptor);
		
		return sdcProvider;
	}
	
	private static ApsSdcProvider addBackboneMetricToChannel(ApsSdcProvider sdcProvider, String channelHandle, AbstractMetricDescriptor metricDescriptor) {
		MdsDescriptor mdsDescriptor = sdcProvider.getMDDescription().getMds().get(0); 
		
		sdcProvider.removeMDS(mdsDescriptor.getHandle()); // remove the "old" MDS
				
		// search for VMD
		for ( VmdDescriptor vmdDescr : mdsDescriptor.getVmd() ) {
			for ( ChannelDescriptor chDescr : vmdDescr.getChannel() ) {
				if ( chDescr.getHandle().equals(channelHandle) ) {
					// add Metric to Channel
					chDescr.getMetric().add(metricDescriptor);
				}
			}
		}
		
		// add MDS to Provider
		sdcProvider.addMDS(mdsDescriptor);
		
		return sdcProvider;
	}


	
	
	
	/**
	 * This method adds numeric metric for every activate operation. 
	 * 
	 * @param sdcProvider
	 * @param mds
	 * @return
	 */
	private static ApsSdcProvider addBackboneMetricsForActivateOperations(ApsSdcProvider sdcProvider, String channelHandle) {
		MdsDescriptor mdsDescriptor = sdcProvider.getMDDescription().getMds().get(0); 
		
		sdcProvider.removeMDS(mdsDescriptor.getHandle()); // remove the "old" MDS
				
		// search for VMD
		ChannelDescriptor configChannelDescr = null;
		for ( VmdDescriptor vmdDescr : mdsDescriptor.getVmd() ) {
			for ( ChannelDescriptor channelDescr : vmdDescr.getChannel() ) {
				if ( channelDescr.getHandle().equals(channelHandle) ) {
					configChannelDescr = channelDescr;
					break;
				}
			}
		}
		
		if ( configChannelDescr != null ) {
			ScoDescriptor scoDescr = mdsDescriptor.getSco();
			if ( scoDescr != null ) {
				for ( AbstractOperationDescriptor aoDesc : scoDescr.getOperation()  ) {
					if ( aoDesc instanceof ActivateOperationDescriptor ) {
						// create a new numeric metric
						NumericMetricDescriptor nmd = new NumericMetricDescriptor();
						nmd.setHandle( Constants.PREFIX_HANDLE_ACT_OP_COUNTER + aoDesc.getHandle() );
						nmd.setMetricCategory(MetricCategory.CLC);
						nmd.setMetricAvailability(MetricAvailability.CONT);
						nmd.setUnit(TypeCodeCalculator.getDimlessCodedValue());
						nmd.setResolution(BigDecimal.ONE);
						
						CodedValue type = new CodedValue();
						type.setCode(Constants.CODE_ACT_OP_COUNT_METRIC);
						nmd.setType(type);
						
						Relation relation = new Relation();
						relation.getEntries().add(aoDesc.getHandle());
						relation.setKind("Oth");
						nmd.getRelation().add(relation);
						
						configChannelDescr.getMetric().add(nmd);
					}
				}
			}
		}
		else {
			// TODO error reprt
		}

		
		// add MDS to Provider
		sdcProvider.addMDS(mdsDescriptor);
		
		return sdcProvider;
	}
		


	/**
	 * This method adds handler for the following kinds of metrics: numeric metric, string metrics 
	 * 
	 * @param sdcProvider
	 * @return
	 */
	private static ApsSdcProvider addHandlerForAllMetrics(ApsSdcProvider sdcProvider) {
		for (MdsDescriptor mdsDescriptor : sdcProvider.getMDDescription().getMds()) {
			for (VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd()) {
				for (ChannelDescriptor channelDescriptor : vmdDescriptor.getChannel()) {
					for (AbstractMetricDescriptor abstractMetricDescriptor : channelDescriptor.getMetric()) {
						if (abstractMetricDescriptor instanceof NumericMetricDescriptor) {
							sdcProvider.addHandler(new NumericMetricStateHandler(abstractMetricDescriptor.getHandle()));
						} else if (abstractMetricDescriptor instanceof EnumStringMetricDescriptor) {
							sdcProvider.addHandler(new EnumStringMetricStateHandler(abstractMetricDescriptor.getHandle()));
						} else if (abstractMetricDescriptor instanceof StringMetricDescriptor) {
							sdcProvider.addHandler(new StringMetricStateHandler(abstractMetricDescriptor.getHandle()));
						}
					}
				}
			}
		}
		return sdcProvider;
	}
	
	/**
	 * This method adds handler for MDS, VMD, and Channel 
	 * 
	 * @param sdcProvider
	 * @return
	 */
	private static ApsSdcProvider addHandlerForAllComplexDeviceComponents(ApsSdcProvider sdcProvider) {

		for (MdsDescriptor mdsDescriptor : sdcProvider.getMDDescription().getMds()) {
			
			sdcProvider.addHandler(new MdsStateHandler(mdsDescriptor.getHandle()));
		
			for (VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd()) {
				
				sdcProvider.addHandler(new VmdStateHandler(vmdDescriptor.getHandle()));
								
				for (ChannelDescriptor channelDescriptor : vmdDescriptor.getChannel()) {
					
					sdcProvider.addHandler(new ChannelStateHandler(channelDescriptor.getHandle()));
					
				}
			}
		}
		return sdcProvider;
	}
	
	/**
	 * This method adds handler for all activate operations 
	 * 
	 * @param sdcProvider
	 * @return
	 * @todo TODO<MVN> method can potentially be added to a util maven package
	 */
	private static ApsSdcProvider addHandlerForAllActivateOperations(ApsSdcProvider sdcProvider, boolean isBackboneInterface) {
		for (MdsDescriptor mdsDescriptor : sdcProvider.getMDDescription().getMds()) {
			if ( mdsDescriptor.getSco() != null ) {
				List<AbstractOperationDescriptor> aodList = mdsDescriptor.getSco().getOperation();
				for (AbstractOperationDescriptor aod : aodList ) {
					if ( aod instanceof ActivateOperationDescriptor ) {
						sdcProvider.addHandler(new ActivateOperationHandler(aod.getHandle(), isBackboneInterface));
					}
				}
			}
		}
		return sdcProvider;
	}

	/**
	 * 
	 * @param sdcProvider
	 * @return
	 * @todo TODO<MVN> method can potentially be added to a util maven package
	 */
	private static ApsSdcProvider addHandlerForAllAlertConditionsAndSignals(ApsSdcProvider sdcProvider) {

		for (MdsDescriptor nextMds : sdcProvider.getMDDescription().getMds()) {
			if (nextMds.getAlertSystem() != null) {
				sdcProvider = addHandlerForAllAlertConditions(nextMds.getAlertSystem(), sdcProvider);
				sdcProvider = addHandlerForAllAlertSignals(nextMds.getAlertSystem(), sdcProvider);
			}
			for (VmdDescriptor nextVmd : nextMds.getVmd())
				if (nextVmd.getAlertSystem() != null) {
					sdcProvider = addHandlerForAllAlertConditions(nextVmd.getAlertSystem(), sdcProvider);
					sdcProvider = addHandlerForAllAlertSignals(nextVmd.getAlertSystem(), sdcProvider);
				}
		}
		return sdcProvider;
	}
	
	/**
	 * Adds state handler for all contexts defined in the description
	 *  
	 * @param sdcProvider
	 * @return
	 * @todo TODO<MVN> method can potentially be added to a util maven package
	 */
	private static ApsSdcProvider addHandlerForAllContexts(ApsSdcProvider sdcProvider) {

		for (MdsDescriptor nextMds : sdcProvider.getMDDescription().getMds()) {
			if (nextMds.getSystemContext() != null) {
				SystemContextDescriptor scd = nextMds.getSystemContext();
				if(scd.getEnsembleContext() != null) {
					List<EnsembleContextDescriptor> ecdList = scd.getEnsembleContext();
					for(EnsembleContextDescriptor ecd : ecdList) {
						sdcProvider.addHandler(new ContextStateHandler<>(EnsembleContextState.class, ecd.getHandle()) );
					}
				}
				if(scd.getLocationContext() != null) {
					LocationContextDescriptor lcd = scd.getLocationContext();
					sdcProvider.addHandler(new ContextStateHandler<>(LocationContextState.class, lcd.getHandle()) );
				}
				if(scd.getMeansContext() != null) {
					List<MeansContextDescriptor> mcdList = scd.getMeansContext();
					for(MeansContextDescriptor mcd : mcdList) {
						sdcProvider.addHandler(new ContextStateHandler<>(MeansContextState.class, mcd.getHandle()) );
					}
				}
				if(scd.getOperatorContext() != null) {
					List<OperatorContextDescriptor> ocdList = scd.getOperatorContext();
					for(OperatorContextDescriptor ocd : ocdList) {
						sdcProvider.addHandler(new ContextStateHandler<>(OperatorContextState.class, ocd.getHandle()) );
					}
				}
				if(scd.getPatientContext() != null) {
					PatientContextDescriptor pcd = scd.getPatientContext();
					sdcProvider.addHandler(new ContextStateHandler<>(PatientContextState.class, pcd.getHandle()) );
				}
				if(scd.getWorkflowContext() != null) {
					List<WorkflowContextDescriptor> wcdList = scd.getWorkflowContext();
					for(WorkflowContextDescriptor wcd : wcdList) {
						sdcProvider.addHandler(new ContextStateHandler<>(WorkflowContextState.class, wcd.getHandle()) );
					}
				}
			}
			else {
				SdcLogger.log.info("There is no Context to handle...");
			}

		}
		return sdcProvider;
	}

	/**
	 * 
	 * @param alSysDesc
	 * @param sdcProvider
	 * @return
	 * @todo TODO<MVN> method can potentially be added to a util maven package
	 */
	private static ApsSdcProvider addHandlerForAllAlertConditions(AlertSystemDescriptor alSysDesc,
			ApsSdcProvider sdcProvider) {
		if (alSysDesc != null) {
			for (AlertConditionDescriptor alConDesc : alSysDesc.getAlertCondition()) {
				if (alConDesc instanceof LimitAlertConditionDescriptor) {
					sdcProvider.addHandler(new LimitAlertConditionStateHandler(alConDesc.getHandle()));
				}
				else if (alConDesc instanceof AlertConditionDescriptor) {
					sdcProvider.addHandler(new AlertConditionStateHandler(alConDesc.getHandle()));
				}
			}
		}
		return sdcProvider;
	}

	/**
	 * searches for alert signals with an alert system and adds a handler to the
	 * provider
	 * 
	 * @param alSysDesc
	 * @param sdcProvider
	 * @return
	 * @todo TODO<MVN> method can potentially be added to a util maven package
	 */
	private static ApsSdcProvider addHandlerForAllAlertSignals(AlertSystemDescriptor alSysDesc, ApsSdcProvider sdcProvider) {
		if (alSysDesc != null) {
			for (AlertSignalDescriptor alSigDesc : alSysDesc.getAlertSignal()) {
				sdcProvider.addHandler(new AlertSignalStateHandler(alSigDesc.getHandle()));
			}
		}
		return sdcProvider;
	}



	/**
	 * Helper method. It is used to create a state with the given current value.
	 * 
	 * @note ComponentActivation is set to ON and Validity to valid
	 * @param correspondingDescriptorHandle
	 *            handle of the descriptor the state belongs to
	 * @param value
	 *            NumericMetricValue of the state
	 * @return a valid state (having the MeasurementValidity "invalid"
	 */
	public static NumericMetricState createInvalidNumericMetricState(String correspondingDescriptorHandle,
			double value) {
		NumericMetricState nms = createNumericMetricState(correspondingDescriptorHandle, value,
				MeasurementValidity.INV);
		return nms;
	}

	/**
	 * Helper method. It is used to create a state with the given current value.
	 * 
	 * @note ComponentActivation is set to ON
	 * @param correspondingDescriptorHandle
	 *            handle of the descriptor the state belongs to
	 * @param value
	 *            NumericMetricValue of the state
	 * @param validity
	 *            Validity of the provided value
	 * @return a valid state
	 */
	public static NumericMetricState createNumericMetricState(String correspondingDescriptorHandle, double value,
			MeasurementValidity validity) {
		// creating an empty numeric metric state
		NumericMetricState nms = new NumericMetricState();
		nms.setActivationState(ComponentActivation.ON);
		nms.setDescriptorHandle(correspondingDescriptorHandle);
		NumericMetricValue nv = new NumericMetricValue();
		nv.setValue(BigDecimal.valueOf(value));
		AbstractMetricValue.MetricQuality amq = new AbstractMetricValue.MetricQuality();
		amq.setValidity(validity);
		nv.setMetricQuality(amq);
		nms.setMetricValue(nv);
		return nms;
	}

	/**
	 * Helper method. It is used to create a state with the given current value.
	 * 
	 * @note ComponentActivation is set to ON and Validity to valid
	 * @param correspondingDescriptorHandle
	 *            handle of the descriptor the state belongs to
	 * @param value
	 *            value of the state
	 * @return a valid state
	 */
	public static StringMetricState createStringMetricState(String correspondingDescriptorHandle, String value) {
		StringMetricState sms = createStringMetricState(correspondingDescriptorHandle, value, MeasurementValidity.VLD, StringMetricState.class);
		return sms;
	}
	
	/**
	 * Helper method. It is used to create a state with the given current value.
	 * 
	 * @note ComponentActivation is set to ON and Validity to valid
	 * @param correspondingDescriptorHandle
	 *            handle of the descriptor the state belongs to
	 * @param value
	 *            value of the state
	 * @return a valid state
	 */
	public static EnumStringMetricState createEnumStringMetricState(String correspondingDescriptorHandle, String value) {
		EnumStringMetricState esms = createStringMetricState(correspondingDescriptorHandle, value, MeasurementValidity.VLD, EnumStringMetricState.class);
		return esms;
	}

	/**
	 * Helper method. It is used to create a state with the given current value.
	 * 
	 * @note ComponentActivation is set to ON
	 * @param correspondingDescriptorHandle
	 *            handle of the descriptor the state belongs to
	 * @param value
	 *            value of the state
	 * @param validity
	 *            Validity of the provided value
	 * @return a valid state
	 */
	public static <T extends StringMetricState> T createStringMetricState(String correspondingDescriptorHandle, String value,
			MeasurementValidity validity, Class<T> clazz) {
		// creating an empty string metric state
		T sms;
		try {
			sms = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			SdcLogger.log.severe("Problem with creating (Enum)StringMetricState!");
			e.printStackTrace();
			return null;
		}
		sms.setActivationState(ComponentActivation.ON);
		sms.setDescriptorHandle(correspondingDescriptorHandle);

		StringMetricValue stringMetricValue = new StringMetricValue();
		stringMetricValue.setValue(value);

		AbstractMetricValue.MetricQuality amq = new AbstractMetricValue.MetricQuality();
		amq.setValidity(validity);
		stringMetricValue.setMetricQuality(amq);

		sms.setMetricValue(stringMetricValue);
		return sms;
	}

	/**
	 * This class handles the state of a numeric metric.
	 * 
	 * @note We do not have to deal with get service requests on our own. This is
	 *       handled by the framework for us.
	 * @author Martin Kasparick (IMD, University of Rostock)
	 * @note Some parts of the code might go back to the examples provided by
	 *       Andreas Besting (SurgiTAIX, Aachen)
	 *
	 */
	private static class NumericMetricStateHandler extends SDCProviderMDStateHandler<NumericMetricState> {

		/**
		 * constructor
		 * 
		 * @param numericMetricHandle
		 *            Handle of the corresponding metric descriptor
		 */
		public NumericMetricStateHandler(String numericMetricHandle) {
			super(numericMetricHandle);
		}

		/**
		 * it is just a delegate method
		 * 
		 * @param value
		 * @return
		 */
		private NumericMetricState createState(double value, MeasurementValidity measurementValidity) {
			return ApsProvicerFactory.createNumericMetricState(getDescriptorHandle(), value, measurementValidity);
		}

		/**
		 * This callback method is called everytime some service consumer requests a
		 * change of the state. This will only happen, if the a operation has been added
		 * to the System Control Object (SCO), by using the
		 * createSetOperationForDescriptor(...) method of the provider
		 */
		@Override
		public InvocationState onStateChangeRequest(NumericMetricState state, OperationInvocationContext oic) {

			BigDecimal valueToSet = ((NumericMetricState) state).getMetricValue().getValue();
			SdcLogger.log.info("Received numeric value change request: " + valueToSet);

			boolean successfulFinished = true;

			if (successfulFinished) {
				return InvocationState.FIN; // Request O.K., let SDCLib/J update
											// internal MDIB
			} else {
				return InvocationState.FAIL; // State will not be updated in
												// internal MDIB
			}

		}

		/**
		 * Initial value of the state (only called at provider startup)
		 */
		@Override
		protected NumericMetricState getInitialState() {
			return createState(0, MeasurementValidity.INV);
		}

		/**
		 * This is the entry point for value changes caused by the real device (or
		 * simulator)
		 * 
		 * @note The provider object itself also offers an updateState method that can
		 *       be used.
		 * 
		 * @param value
		 */
		@SuppressWarnings("unused")
		public final void setValue(double value, MeasurementValidity measurementValidity) {
			NumericMetricState state = createState(value, measurementValidity);
			// Update state in internal MDIB and notify consumers (MDIB version
			// will be increased)
			updateState(state);
		}

	}

	
	/**
	 * This class handles the state of a string metric.
	 *
	 */
	private static class StringMetricStateHandler extends SDCProviderMDStateHandler<StringMetricState> {
		
		/**
		 * constructor
		 * 
		 * @param metricHandle
		 *            Handle of the corresponding metric descriptor
		 */
		public StringMetricStateHandler(String metricHandle) {
			super(metricHandle);
		}

		/**
		 * it is just a delegate method
		 * 
		 * @param value
		 * @return
		 */
		private StringMetricState createState(String value) {
			return ApsProvicerFactory.createStringMetricState(getDescriptorHandle(), value);
		}

		/**
		 * This callback method is called everytime some service consumer requests a
		 * change of the state. This will only happen, if the a operation has been added
		 * to the System Control Object (SCO), by using the
		 * createSetOperationForDescriptor(...) method of the provider
		 */
		@Override
		public InvocationState onStateChangeRequest(StringMetricState state, OperationInvocationContext oic) {

			String valueToSet = ((StringMetricState) state).getMetricValue().getValue();

			SdcLogger.log.info("Received string value change request: " + valueToSet);
			
			boolean successfulFinished = true;
			
			if (state.getDescriptorHandle().equals(Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_STATE_VERSION)) {
				MessageManipulationConfig.getInstance().setStateVersionSingleStateVersion(valueToSet);
				((ApsSdcProvider) provider).updateState(state, true, false);
				successfulFinished = false;
				
				
			} else if (state.getDescriptorHandle().equals(Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_DESC_HANDLE)) {
				MessageManipulationConfig.getInstance().setStateVersionSingleDescHandle(valueToSet);
				((ApsSdcProvider) provider).updateState(state, true, false);
				
				if ( valueToSet.equals("") ) {
					MessageManipulationConfig.getInstance().setManipulationEnabled(false);
				}
				else {
					// FIXME[MK] check whether it is a valid value
					MessageManipulationConfig.getInstance().setManipulationEnabled(true);
				}
				
				successfulFinished = false;
			}
			
				
			if (successfulFinished) {
				return InvocationState.FIN; // Request O.K., let SDCLib/J update
											// internal MDIB
			} else {
				return InvocationState.FAIL; // State will not be updated in
												// internal MDIB
			}

		}

		/**
		 * Initial value of the state (only called at provider startup)
		 */
		@Override
		protected StringMetricState getInitialState() {
			return createState("initial value");
		}

		/**
		 * This is the entry point for value changes caused by the real device (or
		 * simulator)
		 * 
		 * @note The provider object itself also offers an updateState method that can
		 *       be used.
		 * 
		 * @param value
		 */
		@SuppressWarnings("unused")
		public final void setValue(String value) {
			StringMetricState state = createState(value);
			// Update state in internal MDIB and notify consumers (MDIB version
			// will be increased)
			updateState(state);
		}
	}

	
	/**
	 * This class handles the state of a string metric.
	 *
	 */
	private static class EnumStringMetricStateHandler extends SDCProviderMDStateHandler<EnumStringMetricState> {
		
		/**
		 * constructor
		 * 
		 * @param metricHandle
		 *            Handle of the corresponding metric descriptor
		 */
		public EnumStringMetricStateHandler(String metricHandle) {
			super(metricHandle);
		}

		/**
		 * it is just a delegate method
		 * 
		 * @param value
		 * @return
		 */
		private EnumStringMetricState createState(String value) {
			return ApsProvicerFactory.createEnumStringMetricState(getDescriptorHandle(), value);
		}

		/**
		 * This callback method is called everytime some service consumer requests a
		 * change of the state. This will only happen, if the a operation has been added
		 * to the System Control Object (SCO), by using the
		 * createSetOperationForDescriptor(...) method of the provider
		 */
		@Override
		public InvocationState onStateChangeRequest(EnumStringMetricState state, OperationInvocationContext oic) {

			String valueToSet = ((StringMetricState) state).getMetricValue().getValue();

			SdcLogger.log.info("Received string value change request: " + valueToSet);
			
			boolean successfulFinished = true;
			
			if (state.getDescriptorHandle().equals(Constants.HANDLE_CONFIG_PROVIDER_OPERATION_STATE_METRIC)) {
				MessageManipulationConfig.getInstance().setStateVersionSingleDescHandle(valueToSet);
				//we do not need to update this state - will be done by the the own implementations of startup() and close()
//				((ApsSdcProvider) provider).updateState(state, true, false);
				
				//FIXME dummy text implementation
				if ( state.getMetricValue().getValue().equals(Constants.DUT_PROVIDER_OPERATION_STATE_NOT_RUNNING) ) {
					try {
						((ApsSdcProvider) provider).getMirrorSdcProvider().close();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else if ( state.getMetricValue().getValue().equals(Constants.DUT_PROVIDER_OPERATION_STATE_RUNNING) ) {
					if ( !((ApsSdcProvider) provider).getMirrorSdcProvider().isRunning() ) {
						// this must be the BB Interface, thus, the mirror interface is the DUT interface that shall be started
						((ApsSdcProvider) provider).getMirrorSdcProvider().startup();
						
						// copy the metrics of the BB into the DUT representation
						((ApsSdcProvider) provider).getMirrorSdcProvider().copyAllMetricStateOfMirrorProvider();
					}
					else {
						SdcLogger.log.severe("Cannot startup DUT Provider! It is already running!");
					}

				}
				
				successfulFinished = false;
			} 
			
			// FIXME check whether value is within AllowedVaules iff request comes via DUT 

				
			if (successfulFinished) {
				return InvocationState.FIN; // Request O.K., let SDCLib/J update
											// internal MDIB
			} else {
				return InvocationState.FAIL; // State will not be updated in
												// internal MDIB
			}

		}

		/**
		 * Initial value of the state (only called at provider startup)
		 */
		@Override
		protected EnumStringMetricState getInitialState() {
			return createState("initial value");
		}

		/**
		 * This is the entry point for value changes caused by the real device (or
		 * simulator)
		 * 
		 * @note The provider object itself also offers an updateState method that can
		 *       be used.
		 * 
		 * @param value
		 */
		@SuppressWarnings("unused")
		public final void setValue(String value) {
			EnumStringMetricState state = createState(value);
			// Update state in internal MDIB and notify consumers (MDIB version
			// will be increased)
			updateState(state);
		}

	}	
	

	/**
	 * Very simple state handler for component states. The only provided
	 * functionality is to set the component activation to ON. (And obviously make
	 * the state accessible for get service, as this is done by the framework for us
	 * and we do not need to implement any methods for this.)
	 * 
	 * @author Martin Kasparick (IMD, University of Rostock)
	 * @note Some parts of the code might go back to the examples provided by
	 *       Andreas Besting (SurgiTAIX, Aachen)
	 */
	static class MdsStateHandler extends SDCProviderMDStateHandler<MdsState> {

		/**
		 * Constructor.
		 * 
		 * @param correspondingDescriptorHandle
		 *            Handle of the corresponding Descriptor
		 */
		public MdsStateHandler(String correspondingDescriptorHandle) {
			super(correspondingDescriptorHandle);
		}

		/**
		 * This method is called at initialization time. Only setting the activation
		 * state to ON.
		 */
		@Override
		protected MdsState getInitialState() {
			MdsState state = new MdsState();
			state.setDescriptorHandle(super.getDescriptorHandle());
			state.setActivationState(ComponentActivation.ON);
			return state;
		}
	}
	
	/**
	 * Very simple state handler for component states. The only provided
	 * functionality is to set the component activation to ON. (And obviously make
	 * the state accessible for get service, as this is done by the framework for us
	 * and we do not need to implement any methods for this.)
	 * 
	 * @author Martin Kasparick (IMD, University of Rostock)
	 * @note Some parts of the code might go back to the examples provided by
	 *       Andreas Besting (SurgiTAIX, Aachen)
	 */
	static class VmdStateHandler extends SDCProviderMDStateHandler<VmdState> {

		/**
		 * Constructor.
		 * 
		 * @param correspondingDescriptorHandle
		 *            Handle of the corresponding Descriptor
		 */
		public VmdStateHandler(String correspondingDescriptorHandle) {
			super(correspondingDescriptorHandle);
		}

		/**
		 * This method is called at initialization time. Only setting the activation
		 * state to ON.
		 */
		@Override
		protected VmdState getInitialState() {
			VmdState state = new VmdState();
			state.setDescriptorHandle(super.getDescriptorHandle());
			state.setActivationState(ComponentActivation.ON);
			return state;
		}
	}
	
	/**
	 * Very simple state handler for component states. The only provided
	 * functionality is to set the component activation to ON. (And obviously make
	 * the state accessible for get service, as this is done by the framework for us
	 * and we do not need to implement any methods for this.)
	 * 
	 * @author Martin Kasparick (IMD, University of Rostock)
	 * @note Some parts of the code might go back to the examples provided by
	 *       Andreas Besting (SurgiTAIX, Aachen)
	 */
	static class ChannelStateHandler extends SDCProviderMDStateHandler<ChannelState> {

		/**
		 * Constructor.
		 * 
		 * @param correspondingDescriptorHandle
		 *            Handle of the corresponding Descriptor
		 */
		public ChannelStateHandler(String correspondingDescriptorHandle) {
			super(correspondingDescriptorHandle);
		}

		/**
		 * This method is called at initialization time. Only setting the activation
		 * state to ON.
		 */
		@Override
		protected ChannelState getInitialState() {
			ChannelState state = new ChannelState();
			state.setDescriptorHandle(super.getDescriptorHandle());
			state.setActivationState(ComponentActivation.ON);
			return state;
		}
	}

	/**
	 * Helper method to create a suitable limit alert condition state. Activation
	 * state is set to "ON" and monitored limits to "ALL"
	 * 
	 * @param descriptorHandle
	 *            handle of the limit alert condition descriptor
	 * @param lower
	 *            lower limit for the alert condition state (this can tighten the
	 *            limit defined in the descriptor, but not weaken it)
	 * @param upper
	 *            upper limit for the alert condition state (this can tighten the
	 *            limit defined in the descriptor, but not weaken it)
	 * @param stepWidth
	 *            stepWidth for the alert condition state (this can tighten the
	 *            limit defined in the descriptor, but not weaken it)
	 * @param alConPresence
	 *            presence of the alert condition
	 * @return
	 */
	public static LimitAlertConditionState createStateLimitAlertConditionState(String descriptorHandle,
			BigDecimal lower, BigDecimal upper, BigDecimal stepWidth, boolean alConPresence) {
		// create new state
		LimitAlertConditionState alertCondition = new LimitAlertConditionState();
		// add die handle of the descriptor
		alertCondition.setDescriptorHandle(descriptorHandle);
		// set the activation state to on -> this means that the condition
		// object is working; do not mix this up with the alert condition
		// presence
		alertCondition.setActivationState(AlertActivation.ON);
		// set the presence
		alertCondition.setPresence(alConPresence);
		// both, violation of upper and lower limit are monitored
		alertCondition.setMonitoredAlertLimits(AlertConditionMonitoredLimits.ALL);
		// set actual condition range
		Range range = new Range();
		range.setLower(lower);
		range.setUpper(upper);
		alertCondition.setLimits(range);

		// return the condition object
		return alertCondition;
	}
	
	/**
	/**
	 * Helper method to create a suitable limit alert condition state. Activation
	 * state is set to "ON"
	 * 
	 * @param descriptorHandle
	 * @param alConPresence
	 * @return
	 */
	public static AlertConditionState createAlertConditionState(String descriptorHandle, boolean alConPresence) {
		// create a new state
		AlertConditionState alertCondition = new AlertConditionState();
		
		// add die handle of the descriptor
		alertCondition.setDescriptorHandle(descriptorHandle);
		
		// set the activation state to on -> this means that the condition
		// object is working; do not mix this up with the alert condition
		// presence
		alertCondition.setActivationState(AlertActivation.ON);
		
		// set the presence
		alertCondition.setPresence(alConPresence);
		
		return alertCondition;
	}

	/**
	 * @todo FIXME I am currently not sure whether this works for all kinds of contexts
	 *
	 * @param <T> type of the context
	 * @todo TODO<MVN> method can potentially be added to a util maven package
	 */
	public static class ContextStateHandler<T extends AbstractContextState>
			extends SDCProviderMDStateHandler<AbstractContextState> {

		private final Class<T> clazz;

		public ContextStateHandler(Class<T> clazz, String handle) {
			super(handle);
			this.clazz = clazz;
		}

		/**
		 * Helper method to create a suitable state
		 * @param descriptorHandle
		 * @param root can be null
		 * @param ext can be null 
		 * @param ctxtAssociation
		 * @param clazz class type that shall be created
		 * @return
		 */
		public static <T extends AbstractContextState> T createState(String descriptorHandle, String root, String ext,
				ContextAssociation ctxtAssociation, Class<T> clazz) {
			try {
				T t = clazz.newInstance();
				
				// add corresponding descriptor handle 
				t.setDescriptorHandle(descriptorHandle);
				// note: AbstractContextState have a mandatory (state) handle 
				t.setHandle(descriptorHandle + "_state");
				
				//define association state 
				t.setContextAssociation(ctxtAssociation);

				// add identification if given
				if (ext == null && root == null) {
					return t;
				}
				InstanceIdentifier instId = new InstanceIdentifier();
				if (root != null) {
					instId.setRoot(root);
				}
				if (ext != null) {
					instId.setExtension(ext);
				}
				t.getIdentification().add(instId);
				return t;
			} catch (InstantiationException | IllegalAccessException ex) {
				SdcLogger.log.severe("Problem while creating a context state!");
			}
			return null;
		}


		@Override
		public InvocationState onStateChangeRequest(AbstractContextState state, OperationInvocationContext oic) {
			SdcLogger.log.info("Received context value change request...");

			return InvocationState.FIN; // Request O.K., let SDCLib/J update internal MDIB and notify consumers
			// return InvocationState.FAILED; // State will not be updated in internal MDIB
		}

		@Override
		protected AbstractContextState getInitialState() {
			return createState(getDescriptorHandle(), null, null, ContextAssociation.NO, clazz);
		}

//		public final void setValueInternal(String ext) {
//			T state = createState(ext);
//			// Update state in internal MDIB and notify consumers (MDIB version will be
//			// increased)
//			updateState(state);
//		}

	}
	
	
	/**
	 * This class handles the state of a limit alert condition.
	 */
	private static class AlertConditionStateHandler
			extends SDCProviderAlertConditionStateHandler<AlertConditionState> {

		public AlertConditionStateHandler(String descriptorHandle) {
			super(descriptorHandle);
		}

		/**
		 * Method is called at initialization time
		 */
		@Override
		protected AlertConditionState getInitialState() {
			// initially create a state with condition presence "false"
				return ApsProvicerFactory.createAlertConditionState(getDescriptorHandle(), false);
		}

		/**
		 * This callback method is called everytime some service consumer requests a
		 * change of the state. This will only happen, if the a operation has been added
		 * to the System Control Object (SCO).
		 */
		@Override
		public InvocationState onStateChangeRequest(AlertConditionState state, OperationInvocationContext oic) {
			SdcLogger.log.warning("Received  new AlertConditionState: " + state.toString() + "-> will be passed...");

			// If there is a real device, or some objects storing the state
			// in parallel, here would be the place to update.

			return InvocationState.FIN; // Request O.K., let SDCLib/J update
										// internal MDIB
			// return InvocationState.CANCELLED; // State will not be updated in
			// internal MDIB
		}
	}	

	/**
	 * This class handles the state of a limit alert condition.
	 */
	private static class LimitAlertConditionStateHandler
			extends SDCProviderAlertConditionStateHandler<LimitAlertConditionState> {

		String limAlConDescHandle = null;

		public LimitAlertConditionStateHandler(String descriptorHandle) {
			super(descriptorHandle);
			this.limAlConDescHandle = descriptorHandle;
		}

		/**
		 * Method is called at initialization time
		 */
		@Override
		protected LimitAlertConditionState getInitialState() {
			// initially, we define the alert condition range to be same as the
			// range defined in the descriptor and the presence to be off (note,
			// the presence handling will be done in sourceHasChanged(...))

			// for the initial state, we take the range values from the
			// descriptor
			AlertConditionDescriptor alConDesc = Util.findAlertConditionDescriptor(sdcProvider, limAlConDescHandle);
			if (alConDesc instanceof LimitAlertConditionDescriptor) {
				LimitAlertConditionDescriptor limAlconDesc = (LimitAlertConditionDescriptor) alConDesc;

				Range range = limAlconDesc.getMaxLimits();

				return ApsProvicerFactory.createStateLimitAlertConditionState(getDescriptorHandle(),
						range.getLower(), range.getUpper(), range.getStepWidth(), false);
			} else {
				SdcLogger.log.severe("ERROR: This is NOT a LIMIT alert condition!");
				// use dummy values for the range
				return ApsProvicerFactory.createStateLimitAlertConditionState(getDescriptorHandle(),
						BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, false);
			}

		}

		/**
		 * This callback method is called everytime some service consumer requests a
		 * change of the state. This will only happen, if the a operation has been added
		 * to the System Control Object (SCO).
		 */
		@Override
		public InvocationState onStateChangeRequest(LimitAlertConditionState state, OperationInvocationContext oic) {
			SdcLogger.log.warning("Received  new LimitAlertConditionState: " + state.toString() + "-> No handling will be performed!");

			// If there is a real device, or some objects storing the state
			// in parallel, here would be the place to update.

			return InvocationState.FIN; // Request O.K., let SDCLib/J update
										// internal MDIB
			// return InvocationState.CANCELLED; // State will not be updated in
			// internal MDIB
		}

		/**
		 * Callback method being invoked whenever the source(s) of the alert condition
		 * has changed. Thus, the limits have to be evaluated here.
		 */
		@Override
		public void sourceHasChanged(AbstractMetricState source, LimitAlertConditionState currentState) {
			// check whether the source is really a numeric metric
			if (!(source instanceof NumericMetricState)) {
				SdcLogger.log.severe("Cannot handle limit alert condition presence as the source is not a numeric metric!");
				return;
			}

			// get the current metric value (with some null handling)
			NumericMetricState nms = (NumericMetricState) source;
			if (nms.getMetricValue() == null) {
				return;
			}
			BigDecimal currentAlConSourceValue;
			if ((currentAlConSourceValue = nms.getMetricValue().getValue()) == null) {
				return;
			}
			double currentMetricValue = currentAlConSourceValue.doubleValue();

			// get the current limit alert condition state
			LimitAlertConditionState lacState = (LimitAlertConditionState) currentState;
			Range limits;
			if ((limits = lacState.getLimits()) == null) {
				return;
			}

			boolean trigger = false;
			if (limits.getUpper() == null && limits.getLower() == null) {
				return;
			} else if (limits.getUpper() == null) {
				// evaluate the condition
				trigger = (currentMetricValue < limits.getLower().doubleValue());
			} else if (limits.getLower() == null) {
				// evaluate the condition
				trigger = (currentMetricValue > limits.getUpper().doubleValue());
			} else {
				// evaluate the condition
				trigger = (currentMetricValue > limits.getUpper().doubleValue()
						|| currentMetricValue < limits.getLower().doubleValue());
			}

			// Hand over the result to the framework that will trigger all
			// signals if necessary
			setAlertConditionPresence(currentState, trigger);
		}
	}

	public static class AlertSignalStateHandler extends SDCProviderMDStateHandler<AlertSignalState> {

		public AlertSignalStateHandler(String descriptorHandle) {
			super(descriptorHandle);
		}

		// Helper method
		public static AlertSignalState createState(String handle) {
			AlertSignalState alertSignal = new AlertSignalState();
			alertSignal.setDescriptorHandle(handle);
			alertSignal.setActivationState(AlertActivation.ON);
			alertSignal.setPresence(AlertSignalPresence.OFF);
			return alertSignal;
		}

		private AlertSignalState createState() {
			return AlertSignalStateHandler.createState(getDescriptorHandle());
		}

		@Override
		public InvocationState onStateChangeRequest(AlertSignalState state, OperationInvocationContext oic) {
			return InvocationState.FIN;
		}

		@Override
		protected AlertSignalState getInitialState() {
			return createState();
		}

	}

	static class ActivateOperationHandler extends SDCProviderActivateOperationHandler {

		String activateOperationDescriptorHandle = null;
		boolean isBackboneInterface = false;

		public ActivateOperationHandler(String activateOperationDescriptorHandle, boolean isBackboneInterface) {
			super(activateOperationDescriptorHandle);
			this.isBackboneInterface = isBackboneInterface;
		}

		@Override
		public InvocationState onActivateRequest(Mdib mdib, OperationInvocationContext oic) {

			String opertionHandle = oic.getOperationHandle();
			SdcLogger.log.info(
					"Received activate request for ActivateOperation with Descriptor Handle: " + opertionHandle);

			boolean successfulFinished = false;
			
			// this part is for handling for MoVE Platform with backbone only
			if ( !Config.getInstance().isNoBackboneInterface() ) {
			
				// if this is the DUT interface, we will have to increment the corresponding 
				// counter metric of the BB interface.
				// Do nothing for BB interface activate operations 
				if ( !isBackboneInterface ) {
					String handleOfInterest = Constants.PREFIX_HANDLE_ACT_OP_COUNTER + opertionHandle;
					List<String> handles = new ArrayList<String>();
					handles.add(handleOfInterest);
					
					if ( sdcProvider.getMDState(handles).getState() != null && 
							!sdcProvider.getMDState(handles).getState().isEmpty() ) {
						AbstractState as = sdcProvider.getMDState(handles).getState().get(0);
						
						if ( as instanceof NumericMetricState ) {
							// get current value
							NumericMetricState nms = (NumericMetricState) as;
							BigDecimal currentValue = nms.getMetricValue().getValue();
							
							// update value -> value++
							nms.getMetricValue().setValue(currentValue.add(BigDecimal.ONE));
							sdcProvider.updateState(nms, true, false); //but do not update the mirror, having no such metric
						}
						else {
							SdcLogger.log.severe("Activate Operation Handling went wrong... NOT a NumericMetricState! :-(");
							successfulFinished = false;
						}
						
						successfulFinished = true;
					}
					else {
						SdcLogger.log.severe("Activate Operation Handling went wrong... :-(");
						successfulFinished = false;
					}
				}
				else {
					SdcLogger.log.severe("Activate Operation received at Backbon Interface -> NOT handled!");
					successfulFinished = false;
				}
			}
			else { // APS as stand-alone tool
				//TODO implement fancy behavior description for activates
				successfulFinished = true;
			}
			
			if (successfulFinished) {
				return InvocationState.FIN; // Request O.K., let SDCLib/J update
											// internal MDIB
			} else {
				SdcLogger.log.severe("ERROR: Activate Operation FAILED!");
				return InvocationState.FAIL; // State will not be updated in
												// internal MDIB
			}

		}
		
	}

}
