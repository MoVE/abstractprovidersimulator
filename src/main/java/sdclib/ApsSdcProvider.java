package sdclib;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractOperationState;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.EnumStringMetricState;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.provider.SDCProvider;

import application.Config;
import localValueSimulator.AbstractApsSimulator;
import logging.SdcLogger;
import util.Constants;


public class ApsSdcProvider extends SDCProvider {
	
	private boolean isBackboneProvider = false;
	
	private ApsSdcProvider mirrorSdcProvider;
	
	private List<AbstractApsSimulator> metricValueSimulators = null;

	public ApsSdcProvider(boolean isBackboneProvider) {
		super();
		this.setBackboneProvider(isBackboneProvider);
		
		if(!isBackboneProvider) {
			metricValueSimulators = new ArrayList<AbstractApsSimulator>();
		}
	}
	
	public void updateState(AbstractState state) {
        updateState(state, true);
    }
	
	public void updateState(AbstractState state, boolean notifyEvents) {   	
		updateState(state, notifyEvents, true);
	}
	
	public void updateState(AbstractState state, boolean notifyEvents, boolean updateMirrorAsWell) {
    	//trigger update of mirror provider
		//avoid ping-pong between the mirrors and there can only be a mirror if there is a BB interface
		if(updateMirrorAsWell && !Config.getInstance().isNoBackboneInterface()) { 
			mirrorSdcProvider.updateState(state, notifyEvents, false);
		}
		
		if ( !this.isRunning() ) {
			SdcLogger.log.finer("Will NOT update - Provider is not running - " + this.getEndpointReference());
			return;
		}
		
		SdcLogger.log.info("Try to updateState() - " + this.getEndpointReference() + " - Descriptor Handle: " + state.getDescriptorHandle());
    	
        if (!updateInternalMatchingState(state)) {
            throw new RuntimeException("No state to update with given descriptor handle (state handle): " + state.getDescriptorHandle());
        }
        // Call transport state changed
        transportBinding.onStateChanged(state);
        if (!notifyEvents) {
            return;
        }
        // Eventing
        if (state instanceof RealTimeSampleArrayMetricState) {
            transportBinding.getEventSourceBinding().handleStream((RealTimeSampleArrayMetricState)state, false);
        }        
        else if (state instanceof AbstractMetricState) {
            transportBinding.getEventSourceBinding().handleEpisodicMetricEvent((AbstractMetricState) state, BigInteger.valueOf(mdibVersion.get()));
            evaluateAlertConditions(((AbstractMetricState) state));
        }
        else if (state instanceof AbstractContextState)
            transportBinding.getEventSourceBinding().handleEpisodicContextChangedEvent((AbstractContextState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof AbstractAlertState)
            transportBinding.getEventSourceBinding().handleEpisodicAlertEvent((AbstractAlertState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof AbstractOperationState)
            throw new RuntimeException("Eventing not yet supported for operation states, handle: " + state.getDescriptorHandle());
    }

	public ApsSdcProvider getMirrorSdcProvider() {
		return mirrorSdcProvider;
	}

	public void setMirrorSdcProvider(ApsSdcProvider mirrorSdcProvider) {
		this.mirrorSdcProvider = mirrorSdcProvider;
	}
	
	@Override
	public void startup() {
		super.startup();
		if ( !isBackboneProvider ) {
			//if the DUT interface is startup, we have to update the BB metric
			EnumStringMetricState esms = SDCToolbox.findState(
					mirrorSdcProvider, Constants.HANDLE_CONFIG_PROVIDER_OPERATION_STATE_METRIC, EnumStringMetricState.class);
			
			if (esms != null) {
				esms.getMetricValue().setValue(Constants.DUT_PROVIDER_OPERATION_STATE_RUNNING);
				mirrorSdcProvider.updateState(esms, true, false);
			}
			else if ( !Config.getInstance().isNoBackboneInterface() ){
				// this error message does only make sense if the BB is intended to be started
				SdcLogger.log.severe("Could NOT declare provider as running. Most like the DUT has been started before the BB!");
			}
		}
	}
	
	@Override
	public void close() {
		
		try {
			super.close();
		} catch (Exception e) {
			SdcLogger.log.severe("Could NOT close() the provider!");
			e.printStackTrace();
			return;
		}
		
		if ( !isBackboneProvider ) {
			//if the DUT interface is startup, we have to update the BB metric
			EnumStringMetricState esms = SDCToolbox.findState(
					mirrorSdcProvider, Constants.HANDLE_CONFIG_PROVIDER_OPERATION_STATE_METRIC, EnumStringMetricState.class);
			
			if (esms != null) {
				esms.getMetricValue().setValue(Constants.DUT_PROVIDER_OPERATION_STATE_NOT_RUNNING);
				mirrorSdcProvider.updateState(esms, true, false);
			} else {
				SdcLogger.log.severe("Could NOT declare provider as running. Most like the DUT has been started before the BB!");
			}
		}
	}
	
	public void copyAllMetricStateOfMirrorProvider() {
		for (MdsDescriptor mdsDescriptor : mirrorSdcProvider.getMDDescription().getMds()) {
			for (VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd()) {
				for (ChannelDescriptor channelDescriptor : vmdDescriptor.getChannel()) {
					for (AbstractMetricDescriptor abstractMetricDescriptor : channelDescriptor.getMetric()) {
						// search the state in this provider
						AbstractMetricState thisAms = SDCToolbox.findMetricState(this, abstractMetricDescriptor.getHandle());
						AbstractMetricState mirrorAms = SDCToolbox.findMetricState(mirrorSdcProvider, abstractMetricDescriptor.getHandle());
						
						// check whether there is a metric in this provider for the metric of the mirror provider
						if ( thisAms != null ) {
							
							long thisSV = thisAms.getStateVersion().longValue();
							long mirrorSV = mirrorAms.getStateVersion().longValue();
							
							long svDiff = mirrorSV - thisSV;
							
							if ( thisAms instanceof NumericMetricState ) {
								NumericMetricState nms = (NumericMetricState) mirrorAms;
								
								//FIXME hack: otherwise DUT has StateVersion 1 and BB 0
								if ( nms.getStateVersion().equals(BigInteger.ZERO) ) {
									mirrorSdcProvider.updateState(nms, true, false);
									updateState(nms, true, false);
								}
								else {
									//FIXME: hell of a hack: sync state versions
									for (long i = 0; i < svDiff; i++ ) {								
										updateState(nms, true, false);
									}
								}			
							} else if ( thisAms instanceof EnumStringMetricState ) {
								EnumStringMetricState esms = (EnumStringMetricState) mirrorAms;
								
								//FIXME hack: otherwise DUT has StateVersion 1 and BB 0
								if ( esms.getStateVersion().equals(BigInteger.ZERO) ) {
									mirrorSdcProvider.updateState(esms, true, false);
									updateState(esms, true, false);
								}
								else {
									//FIXME: hell of a hack: sync state versions
									for (long i = 0; i < svDiff; i++ ) {								
										updateState(esms, true, false);
									}
								}
							} else if ( thisAms instanceof StringMetricState ) {
								StringMetricState sms = (StringMetricState) mirrorAms;
								
								//FIXME hack: otherwise DUT has StateVersion 1 and BB 0
								if ( sms.getStateVersion().equals(BigInteger.ZERO) ) {
									mirrorSdcProvider.updateState(sms, true, false);
									updateState(sms, true, false);
								}
								else {
									//FIXME: hell of a hack: sync state versions
									for (long i = 0; i < svDiff; i++ ) {
										updateState(sms, true, false);
									}
								}
							} else {
								SdcLogger.log.severe("Did NOT copy this metric state as it as an unexpected type!");
							}
						}
					}
				}
			}
		}
	}

	public boolean isBackboneProvider() {
		return isBackboneProvider;
	}

	public void setBackboneProvider(boolean isBackboneProvider) {
		this.isBackboneProvider = isBackboneProvider;
	}

	/**
	 * adds a new metric value Simulator to the list of available simulators 
	 * @param sim
	 */
	public void addMetricValueSimulator(AbstractApsSimulator sim) {
		if (!isBackboneProvider) {
			metricValueSimulators.add(sim);
		}
		else {
			SdcLogger.log.severe("NOT supported for Backbone Provider!");
		}
	}
	
	/**
	 * 
	 * @return List of available metric simulators
	 */
	public List<AbstractApsSimulator> getMetricValueSimulatorList() {
		return metricValueSimulators;
	}
}
