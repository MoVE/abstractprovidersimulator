package sdclib.util;

import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.sdclib.provider.SDCEndpoint;

import logging.SdcLogger;

/**
 * This class provides convenience functions
 * 
 * @author Martin Kasparick (IMD, University of Rostock)
 *
 */
public class Util {
	
	public static final int MDC_DIM_DIMLESS = 512;

	/**
	 * IEEE 11073-1010x block number for infrastructure codes, e.g., MDS, VMD,
	 * Channel
	 */
	public static final int BLOCK_NUM_INFRASTRUCTURE = 1;

	/**
	 * IEEE 11073-1010x block number for SCADA (Physio IDs), e.g., metrics
	 */
	public static final int BLOCK_NUM_SCADA = 2;
	
	/**
	 * IEEE 11073-1010x block number for events and alerts
	 */
	public static final int BLOCK_NUM_EVENT = 3;
	
	/**
	 * IEEE 11073-1010x block number for Dimensions, e.g., units
	 */
	public static final int BLOCK_NUM_DIMENSION = 4;
	
	/**
	 * IEEE 11073-1010x "private" block number
	 */
	public static final int BLOCK_NUM_PRIVATE = 1024;

	/**
	 * calculation of type code in IEEE 11073-1010x: block number * 2^16 + term
	 * code
	 * 
	 * @param termCode
	 *            basic term code that can be found in the nomenclature standard
	 * @param blockNumber
	 *            block number belonging to the basic term code
	 * @return
	 */
	public static String calcTypeCode(int termCode, int blockNumber) {
		return (blockNumber * ((int) Math.pow(2, 16)) + termCode) + "";
	}

	/**
	 * Calculates type code with fix block number = 1
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcInfrastructureTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_INFRASTRUCTURE);
	}
	
	/**
	 * Calculates type code with fix block number = 2
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcScadaTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_SCADA);
	}

	/**
	 * Calculates type code with fix block number = 3
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcEventTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_EVENT);
	}
	
	/**
	 * Calculates type code with fix block number = 4
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcDimensionTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_DIMENSION);
	}
	
	/**
	 * Calculates type code with fix block number = 1024
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcTypeCodePrivateBlock(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_PRIVATE);
	}
	
	public static AlertConditionDescriptor findAlertConditionDescriptor(SDCEndpoint ep, String handle) {
        
		AlertConditionDescriptor ret = null;
		
		MdDescription mdd = ep.getMDDescription();
        for (MdsDescriptor nextMds : mdd.getMds()) {
        	if(nextMds.getAlertSystem() != null) {
        		ret = findAlertConditionDescriptor(nextMds.getAlertSystem(), handle);
        		if( ret != null){
        			return ret;
        		}
        	}
            for (VmdDescriptor nextVmd : nextMds.getVmd())
            	if(nextVmd.getAlertSystem() != null) {
            		ret = findAlertConditionDescriptor(nextVmd.getAlertSystem(), handle);
            		if( ret != null){
            			return ret;
            		}
            	}
        }
        
		SdcLogger.log.severe("ERROR: Did NOT find any alert condition descriptor for the given handle: " + handle);
        return null;
    }
	
	public static AlertConditionDescriptor findAlertConditionDescriptor(AlertSystemDescriptor alSysDesc, String alConDescHandle){
		for( AlertConditionDescriptor acd : alSysDesc.getAlertCondition()){
			if(acd.getHandle().equals(alConDescHandle)){
				return acd;
			}
		}
		
		return null;
	}
	
}
