package util;

import org.ornet.cdm.CodedValue;

/**
 * This class provides convenience functions
 * 
 * @author Martin Kasparick (IMD, University of Rostock)
 *
 */
public class TypeCodeCalculator {

	/**
	 * IEEE 11073-1010x block number for infrastructure codes, e.g., MDS, VMD,
	 * Channel
	 */
	public static final int BLOCK_NUM_INFRASTRUCTURE = 1;

	/**
	 * IEEE 11073-1010x block number for SCADA (Physio IDs), e.g., metrics
	 */
	public static final int BLOCK_NUM_SCADA = 2;
	
	/**
	 * IEEE 11073-1010x block number for events and alerts
	 */
	public static final int BLOCK_NUM_EVENT = 3;
	
	/**
	 * IEEE 11073-1010x block number for Dimensions, e.g., units
	 */
	public static final int BLOCK_NUM_DIMENSION = 4;

	/**
	 * calculation of type code in IEEE 11073-1010x: block number * 2^16 + term
	 * code
	 * 
	 * @param termCode
	 *            basic term code that can be found in the nomenclature standard
	 * @param blockNumber
	 *            block number belonging to the basic term code
	 * @return
	 */
	public static String calcTypeCode(int termCode, int blockNumber) {
		return (blockNumber * ((int) Math.pow(2, 16)) + termCode) + "";
	}

	/**
	 * Calculates type code with fix block number = 1
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcInfrastructureTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_INFRASTRUCTURE);
	}
	
	/**
	 * Calculates type code with fix block number = 2
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcScadaTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_SCADA);
	}

	/**
	 * Calculates type code with fix block number = 3
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcEventTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_EVENT);
	}
	
	/**
	 * Calculates type code with fix block number = 4
	 * 
	 * @param termCode
	 * @return
	 */
	public static String calcDimensionTypeCode(int termCode) {
		return calcTypeCode(termCode, BLOCK_NUM_DIMENSION);
	}
	
	/**
	 * 
	 * @return Coded Value representing the unit "dimless"
	 */
	public static CodedValue getDimlessCodedValue() {
		CodedValue dimless = new CodedValue();
		dimless.setCode(TypeCodeCalculator.calcDimensionTypeCode(512));
		dimless.setSymbolicCodeName("MDC_DIM_DIMLESS");
		
		return dimless;
	}
	
}

