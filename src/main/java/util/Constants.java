package util;

public class Constants {
	
	public static final String PREFIX = "b39e6544-bf95-4727-8e3b-5741757df209";
	
	public static final String CODE_ACT_OP_COUNT_METRIC = "23456";
	
	public static final String PREFIX_HANDLE_ACT_OP_COUNTER = "h_counter_metric_";
	
	
	// ### Constants for message manipulation	
	public static final String HANDLE_MSG_MANIPULATION_VMD = 
			"h_bb_vmd_msg_manipulation";
	
	public static final String HANDLE_MSG_MANIPULATION_CHANNEL = 
			"h_bb_ch_msg_manipultion_single_state_version";
	
	public static final String HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_DESC_HANDLE = 
			"h_msg_manipulate_state_version_desc_handle";
	
	public static final String HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_STATE_VERSION = 
			"h_msg_manipulate_state_version_version_num";
	
	// ### Constants for APS configuration
	public static final String HANDLE_CONFIG_VMD = "h_aps_config_vmd";
	
	public static final String HANDLE_CONFIG_PROVIDER_STATE_CHANNEL = 
			"h_aps_config_provider_state_channel";
	
	public static final String HANDLE_CONFIG_PROVIDER_OPERATION_STATE_METRIC =
			"h_aps_config_provider_operation_state_metric";
	
	public static final String DUT_PROVIDER_OPERATION_STATE_RUNNING = "running";
	public static final String DUT_PROVIDER_OPERATION_STATE_NOT_RUNNING = "not_running";

}
