package util;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import application.Config;
import logging.SdcLogger;
import move.imd.MoveExtensionConfig;

/**
 * 
 * @author Martin
 * @see https://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
 */
public class DomXmlConfigReader {

	public static final String TAG_SDCLIB_LOG_LEVEL = "sdclib_log_level";
	
	public static final String TAG_BB_USE_TLS = "backbone_interface_use_tls";
	public static final String TAG_DUT_USE_TLS = "dut_interface_use_tls";
	public static final String TAG_BB_INTERFACE_IP = "backbone_interface_ip_address";
	public static final String TAG_DUT_INTERFACE_IP = "dut_interface_ip_address";
	public static final String TAG_BB_INTERFACE_START_PORT = "backbone_interface_start_port";
	public static final String TAG_DUT_INTERFACE_START_PORT = "dut_interface_ip_start_port";
	public static final String TAG_BB_INTERFACE_EPR = "backbone_interface_endpointreference";
	public static final String TAG_DUT_INTERFACE_EPR = "dut_interface_endpointreference";
	public static final String TAG_NO_BB_INTERFACE = "use_without_backbone_interface";
	public static final String TAG_DUT_AUTOSTART = "dut_interface_autostart";
	
	public static final String TAG_USE_PLAIN_MSG_DISTRIBUTION = "use_plain_message_distribution";
	public static final String TAG_PLAIN_MSG_DISTRIBUTION_TCP_SERVER_PORT = "plain_message_reporting_tcp_server_port";
	
	public static final String TAG_USE_VALUE_SIM = "use_value_simulation";
	public static final String TAG_VALUE_SIM_UPDATE_PERIOD = "value_simulation_update_period";


	public boolean readConfigFromXmlFile(String filepath) {
		File configFile = new File(filepath);

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(configFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			try {
				Config config = Config.getInstance();
				
				try {
					config.setSdclibLogLevel(Level.parse(doc.getElementsByTagName(TAG_SDCLIB_LOG_LEVEL).item(0).getTextContent()));
				}
				catch (Exception e) {
					SdcLogger.log.severe("Error! Could NOT parse Log Level for SDCLib/J output in config file: " + filepath);
				}
				
				try {
					if ( doc.getElementsByTagName(TAG_BB_USE_TLS).getLength() != 0 ) {
						config.setBackboneInterfaceUseTLS(Boolean.parseBoolean(doc.getElementsByTagName(TAG_BB_USE_TLS).item(0).getTextContent()));
					}
				}
				catch (Exception e) {
					SdcLogger.log.severe("Error! Could NOT parse boolean for use TLS flag for Backbone Interface in config file: " + filepath);
				}
				
				try {
					config.setDutInterfaceUseTLS(Boolean.parseBoolean(doc.getElementsByTagName(TAG_DUT_USE_TLS).item(0).getTextContent()));
				}
				catch (Exception e) {
					SdcLogger.log.severe("Error! Could NOT parse boolean for use TLS flag for DUT Interface in config file: " + filepath);
				}

				try {
					if( doc.getElementsByTagName(TAG_DUT_AUTOSTART).getLength() != 0 ) {
						config.setDutInterfaceAutoStart(Boolean.parseBoolean(doc.getElementsByTagName(TAG_DUT_AUTOSTART).item(0).getTextContent()));
					}
				}
				catch (Exception e) {
					SdcLogger.log.severe("Error! Could NOT parse boolean for auto Start configuration of the DUT Interface in config file: " + filepath);
				}
				
				if ( doc.getElementsByTagName(TAG_BB_INTERFACE_IP).getLength() != 0 ) {
					config.setBackboneInterfaceIp(doc.getElementsByTagName(TAG_BB_INTERFACE_IP).item(0).getTextContent());
				}
				config.setDutInterfaceIp(doc.getElementsByTagName(TAG_DUT_INTERFACE_IP).item(0).getTextContent());

				try {
					if ( doc.getElementsByTagName(TAG_BB_INTERFACE_START_PORT).getLength() != 0 ) {
						config.setBackboneInterfaceStartPort(
								Integer.parseInt(doc.getElementsByTagName(TAG_BB_INTERFACE_START_PORT).item(0).getTextContent()));
					}
					config.setDutInterfaceStartPort(
							Integer.parseInt(doc.getElementsByTagName(TAG_DUT_INTERFACE_START_PORT).item(0).getTextContent()));
				}
				catch (NumberFormatException e) {
					SdcLogger.log.severe("Error! Could NOT parse port number in config file: " + filepath);
					return false;
				}
				
				if ( doc.getElementsByTagName(TAG_BB_INTERFACE_EPR).getLength() != 0 ) {
					config.setBackboneInterfaceEpr(doc.getElementsByTagName(TAG_BB_INTERFACE_EPR).item(0).getTextContent());
				}
				config.setDutInterfaceEpr(doc.getElementsByTagName(TAG_DUT_INTERFACE_EPR).item(0).getTextContent());
				
				
				if ( doc.getElementsByTagName(TAG_NO_BB_INTERFACE).getLength() != 0 ) {
					config.setNoBackboneInterface(Boolean.parseBoolean(doc.getElementsByTagName(TAG_NO_BB_INTERFACE).item(0).getTextContent()));
				}
				
					
				
				try {
					if ( doc.getElementsByTagName(TAG_USE_PLAIN_MSG_DISTRIBUTION).getLength() != 0 ) {
						MoveExtensionConfig.getInstance().setUsePlainMessageDistribution(
								Boolean.parseBoolean(doc.getElementsByTagName(TAG_USE_PLAIN_MSG_DISTRIBUTION).item(0).getTextContent()));
					}
				}
				catch (Exception e) {
					SdcLogger.log.severe("Error! Could NOT parse boolean for plain message distribution flag in config file: " + filepath);
				}
				
				try {
					if ( doc.getElementsByTagName(TAG_PLAIN_MSG_DISTRIBUTION_TCP_SERVER_PORT).getLength() != 0 ) {
						MoveExtensionConfig.getInstance().setPlainMessageDistributionTcpServerPort(
								Integer.parseInt(doc.getElementsByTagName(TAG_PLAIN_MSG_DISTRIBUTION_TCP_SERVER_PORT).item(0).getTextContent()));
					}
				}
				catch (NumberFormatException e) {
					SdcLogger.log.severe("Error! Could NOT parse port number for the plain message distribution TCP server in config file: " + filepath);
					return false;
				}
				
				try {
					config.setUseValueSimulation(Boolean.parseBoolean(doc.getElementsByTagName(TAG_USE_VALUE_SIM).item(0).getTextContent()));
				}
				catch (Exception e) {
					SdcLogger.log.severe("Error! Could NOT parse boolean to turn on/off value simulation: " + filepath);
				}
				
				try {
					if (doc.getElementsByTagName(TAG_VALUE_SIM_UPDATE_PERIOD).getLength() != 0 ) {
						config.setValueSimulationUpdatePeriod(Integer.parseInt(doc.getElementsByTagName(TAG_VALUE_SIM_UPDATE_PERIOD).item(0).getTextContent()));
					}
				}
				catch (NumberFormatException e) {
					SdcLogger.log.severe("Error! Could NOT parse value simulation update period: " + filepath);
					return false;
				}
				

			} catch (NullPointerException e) {
				SdcLogger.log
						.severe("Error! One of the necessary parameter is NOT provided in config file: " + filepath);
				return false;
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

}
