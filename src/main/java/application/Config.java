package application;

import util.DomXmlConfigReader;

import java.util.logging.Level;

import logging.SdcLogger;

public class Config {
	
	private static Config config = null;
	
	private String backboneInterfaceIp = "0.0.0.0";
	private String dutInterfaceIp = "0.0.0.0";
	private int backboneInterfaceStartPort = 10000;
	private int dutInterfaceStartPort = 20000;
	private String backboneInterfaceEpr = "MoVE-Backbone";
	private String dutInterfaceEpr = "DUT-Interface";
	
	/**
	 * indicates whether the MoVE Backbone interface is used.
	 * default = true -> no backbone interface
	 */
	private boolean noBackboneInterface = true;
	private boolean backboneInterfaceUseTLS = false;
	private boolean dutInterfaceUseTLS = false;
	
	/**
	 * parameter defining whether the DUT interface is started automatically (true) or via BB interface (false)
	 */
	private boolean dutInterfaceAutoStart = true;
	private Level sdclibLogLevel = Level.FINEST;
	
	private boolean useValueSimulation = true;
	
	@Deprecated
	private int valueSimulationUpdatePeriod = 5000;
	
	private Config() {
	}
	
	public static Config getInstance() {
		if ( config == null ) {
			config = new Config();
		}
		
		return config;
	}

	/**
	 * read config information from file "config.xml"
	 * @return
	 */
	public boolean readXmlConfig(String configFilepath) {
		DomXmlConfigReader configReader = new DomXmlConfigReader();
		if ( !configReader.readConfigFromXmlFile(configFilepath) ) {
			SdcLogger.log.severe("Reading config file was NOT successful!");
			return false;
		}
		
		return true;
	}
	
	public String getBackboneInterfaceIp() {
		return backboneInterfaceIp;
	}

	public void setBackboneInterfaceIp(String backboneInterfaceIp) {
		this.backboneInterfaceIp = backboneInterfaceIp;
	}

	public String getDutInterfaceIp() {
		return dutInterfaceIp;
	}

	public void setDutInterfaceIp(String dutInterfaceIp) {
		this.dutInterfaceIp = dutInterfaceIp;
	}

	public int getBackboneInterfaceStartPort() {
		return backboneInterfaceStartPort;
	}

	public void setBackboneInterfaceStartPort(int backboneInterfaceStartPort) {
		this.backboneInterfaceStartPort = backboneInterfaceStartPort;
	}

	public int getDutInterfaceStartPort() {
		return dutInterfaceStartPort;
	}

	public void setDutInterfaceStartPort(int dutInterfaceStartPort) {
		this.dutInterfaceStartPort = dutInterfaceStartPort;
	}

	public String getBackboneInterfaceEpr() {
		return backboneInterfaceEpr;
	}

	public void setBackboneInterfaceEpr(String backboneInterfaceEpr) {
		this.backboneInterfaceEpr = backboneInterfaceEpr;
	}

	public String getDutInterfaceEpr() {
		return dutInterfaceEpr;
	}

	public void setDutInterfaceEpr(String dutInterfaceEpr) {
		this.dutInterfaceEpr = dutInterfaceEpr;
	}

	public boolean isNoBackboneInterface() {
		return noBackboneInterface;
	}

	public void setNoBackboneInterface(boolean noBackboneInterface) {
		this.noBackboneInterface = noBackboneInterface;
	}

	public boolean isBackboneInterfaceUseTLS() {
		return backboneInterfaceUseTLS;
	}

	public void setBackboneInterfaceUseTLS(boolean backboneInterfaceUseTLS) {
		this.backboneInterfaceUseTLS = backboneInterfaceUseTLS;
	}

	public boolean isDutInterfaceUseTLS() {
		return dutInterfaceUseTLS;
	}

	public void setDutInterfaceUseTLS(boolean dutInterfaceUseTLS) {
		this.dutInterfaceUseTLS = dutInterfaceUseTLS;
	}

	public Level getSdclibLogLevel() {
		return sdclibLogLevel;
	}

	public void setSdclibLogLevel(Level sdclibLogLevel) {
		this.sdclibLogLevel = sdclibLogLevel;
	}

	public boolean isUseValueSimulation() {
		return useValueSimulation;
	}

	public void setUseValueSimulation(boolean useValueSimulation) {
		this.useValueSimulation = useValueSimulation;
	}

	@Deprecated
	public int getValueSimulationUpdatePeriod() {
		return valueSimulationUpdatePeriod;
	}

	@Deprecated
	public void setValueSimulationUpdatePeriod(int valueSimulationUpdatePeriod) {
		this.valueSimulationUpdatePeriod = valueSimulationUpdatePeriod;
	}

	public boolean isDutInterfaceAutoStart() {
		return dutInterfaceAutoStart;
	}

	public void setDutInterfaceAutoStart(boolean dutInterfaceAutoStart) {
		this.dutInterfaceAutoStart = dutInterfaceAutoStart;
	}
}
