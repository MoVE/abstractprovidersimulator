package application;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class UserInterfaceHandler extends Application {

	@FXML public TextField txt_value;

	public static UserInterfaceHandler ui;
	public static boolean active = false;

	@Override
	public void init() {}

	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("ui.fxml"));
			loader.setController(this);
			VBox root = loader.load();
			ui = this;
			active = true;
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getClassLoader().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("APSBloodPressure");
			// can only be left with keyboard
			// primaryStage.setFullScreen(true);
			primaryStage.show();
		} catch (Exception e) {
			stop();
		}
	}

	@Override
	public void stop() {
		System.exit(0);
	}

	public void setText(String text) {
		if (UserInterfaceHandler.active) {
			this.txt_value.setText(text);
		}
	}

	public static void startUi(String[] args) {
		launch(args);
	}
}
