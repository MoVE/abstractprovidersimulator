package application;

public class APSUiApplication  {
	
	public static void main(String[] args) {
		new Thread() {
			public void run() {
				APSApplication.main(args);
			}
		}.start();
	
		UserInterfaceHandler.startUi(args);
	}
}