package application;

import java.math.BigDecimal;
import java.util.Scanner;

import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.binding.mdpws.MDPWSTransportLayerConfiguration;
import org.ornet.sdclib.binding.mdpws.MDPWSTransportLayerDetail;

import localValueSimulator.AbstractApsSimulator;
import localValueSimulator.ApsValueSimulator;
import logging.SdcLogger;
import mdibLoading.MdibFileReader;
import move.imd.MoveExtensionConfig;
import sdclib.ApsProvicerFactory;
import sdclib.ApsSdcProvider;

public class APSApplication {
	
	static ApsValueSimulator simulator = null;

	public static void main(String[] args) {
		
			SdcLogger.log.info("MoVE Abstract Provider Simulator (APS) - Version: " + APSApplication.class.getPackage().getImplementationVersion());
        
		// the MDIB file to be loaded shall be submitted as a command line argument
		if(args.length != 2) {
			SdcLogger.log.severe("Define MDIB File and Config File to load as a command line argument!");
			SdcLogger.log.severe("Usage: java -jar <File Name> <MDIB File> <Config File>");
			return;
		}
		
		String mdibFilePathName = args[0];
		String configFilePath = args[1];
		
		
		// Read config file
		if ( !Config.getInstance().readXmlConfig(configFilePath) ) {
			return;
		}
		

		// load configuration from file
		SdcLogger.log.info("Load APS config:");
		if( !Config.getInstance().isNoBackboneInterface() ) {
			SdcLogger.log.info("   Backbone Interface Adresse: " + Config.getInstance().getBackboneInterfaceIp());
		}
		SdcLogger.log.info("   DUT Networ Interface Adresse: " + Config.getInstance().getDutInterfaceIp());
		
		SdcLogger.log.info("MoVE Extension for Plain Message Distribution: ");
		SdcLogger.log.info("   Use Plain Message Distribution: " + MoveExtensionConfig.getInstance().isUsePlainMessageDistribution());
		SdcLogger.log.info("   TCP Server Port for Plain Message Distribution: " + MoveExtensionConfig.getInstance().getPlainMessageDistributionTcpServerPort());
		

		
		//try to read the MDIB file
		MdibFileReader mfr = new MdibFileReader();
		String mdibContent = mfr.readMdibFromXmlFile(mdibFilePathName);
		
		if(mdibContent == null) {
			return;
		}
		
		// start the SDCLib/J framework
		SdcLogger.log.info("Starting SDCLib/J...");
		SDCLib.getInstance().startup(false); //start without initial discovery, as this is a provider
		SDCLib.getInstance().setSchemaValidationEnabled(true);
		
	
		SDCLib.getInstance().setLogLevel(Config.getInstance().getSdclibLogLevel());
		
		MDPWSTransportLayerDetail configuration = SDCLib.getInstance().getDefaultTransportLayerConfig(MDPWSTransportLayerConfiguration.class).getConfigurationDetail();
        configuration.setPortStart(30000);
		
		ApsSdcProvider providerBB = new ApsSdcProvider(true);
		ApsSdcProvider providerDut = new ApsSdcProvider(false);
		
		ApsProvicerFactory.initMirroredProviders(providerDut, providerBB, mdibContent, Config.getInstance().getDutInterfaceEpr(), Config.getInstance().getBackboneInterfaceEpr());
		
		if( !Config.getInstance().isNoBackboneInterface() ) {
			providerBB.startup();
		}
		if ( Config.getInstance().isDutInterfaceAutoStart() ) {
			providerDut.startup();
		}
		
		SdcLogger.log.info("SDCLib/J Abstract Provider Simulator started...");
		
		// start simulator
		if(Config.getInstance().isUseValueSimulation()) {
			//old simulator
//			simulator = new ApsValueSimulator(providerDut);
//			simulator.start();
			
			for(AbstractApsSimulator sim : providerDut.getMetricValueSimulatorList()) {
				sim.start();
			}
		}
		
		waitForUserInput(providerDut);

	}
	
	/**
	 * Waiting for user input to change metric values.
	 * @param providerDut
	 */
	private static void waitForUserInput(ApsSdcProvider providerDut) {
		
		Scanner scanner = new Scanner(System.in);
		String readLineFromConsole = ""; 
		String descriptorHandle = "";
		float valueToSet;
		
		while(true) {
			System.out.println("If necessary, you can change numeric metric value via console input.");
			System.out.println("Insert: <descriptor handle> <value to set>");
			
			readLineFromConsole = scanner.nextLine();
			if (readLineFromConsole.equals("exit")) {
				break;
			}
			else if (readLineFromConsole.equals("start")) {
				periodicIncrementNumericMetric(providerDut, "m_bp_sys");
			}
			
			// if there is input, stop simulation
			if (simulator != null ) {
				simulator.stop();
			}
			
			// parse input string
			String[] splitInput = readLineFromConsole.split(" ");			
			if ( splitInput.length == 2 ) {
				try {
					valueToSet = Float.parseFloat(splitInput[1]);
					descriptorHandle = splitInput[0];
					// get the current state for the given descriptor handle
					AbstractMetricState ams = SDCToolbox.findMetricState(providerDut, descriptorHandle);
					if ( ams != null  ) {
						if ( ams instanceof NumericMetricState ) {
							NumericMetricState nms = (NumericMetricState) ams;
							// update numeric metric value
							if ( nms.getMetricValue() != null ) {
								nms.getMetricValue().setValue(BigDecimal.valueOf(valueToSet));
								if ( Config.getInstance().isNoBackboneInterface() ) {
									providerDut.updateState(nms, true, false);
								}
								else {
									providerDut.updateState(nms);
								}
								
								if(UserInterfaceHandler.active && nms.getDescriptorHandle().equals("m_bp_sys")){
									UserInterfaceHandler.ui.setText(nms.getMetricValue().getValue().toString());
								}
							}
							else {
								System.err.println("ERROR: There is no metric value for this metric!");
							}
						}
						else {
							System.err.println("ERROR: specified metric is NOT a numeric metric!");
						}
					}
					else {
						System.err.println("ERROR: there is no metric with the given descriptor handle!");
					}
				}
				catch( NumberFormatException nfe ) {
					System.err.println("ERROR: incorrect input! (Number cannot be parsed!)");
				}
			}
			else {
				System.err.println("ERROR: incorrect input!");
			}
		}
		
		scanner.close();
	}
	
	private static void periodicIncrementNumericMetric(ApsSdcProvider providerDut, String descriptorHandle) {
		AbstractMetricState ams = SDCToolbox.findMetricState(providerDut, descriptorHandle);
		BigDecimal valueToSet = BigDecimal.ZERO;
		if ( ams != null  ) {
			if ( ams instanceof NumericMetricState ) {
				NumericMetricState nms = (NumericMetricState) ams;
				// update numeric metric value
				if ( nms.getMetricValue() != null ) {
					nms.getMetricValue().setValue(valueToSet);
					
					while (true ) {
						
						nms.getMetricValue().setValue(valueToSet);
						System.err.println("Setting: " + nms.getMetricValue().getValue().toString());
						System.err.println("Setting: " + valueToSet);
						
						if ( Config.getInstance().isNoBackboneInterface() ) {
							providerDut.updateState(nms, true, false);
						}
						else {
							providerDut.updateState(nms);
						}
						
						valueToSet = valueToSet.add(BigDecimal.ONE);
						try {
							Thread.sleep(250);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				else {
					System.err.println("ERROR: There is no metric value for this metric!");
				}
			}
			else {
				System.err.println("ERROR: specified metric is NOT a numeric metric!");
			}
		}
		else {
			System.err.println("ERROR: there is no metric with the given descriptor handle!");
		}
	}

}
