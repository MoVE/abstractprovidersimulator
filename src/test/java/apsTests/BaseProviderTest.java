package apsTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.binding.mdpws.MDPWSTransportLayerConfiguration;
import org.ornet.sdclib.binding.mdpws.MDPWSTransportLayerDetail;
import org.ornet.sdclib.common.SyncEvent;
import org.ornet.sdclib.consumer.SDCConsumer;
import org.ornet.sdclib.consumer.SDCLifecycleHandler;

import application.Config;
import logging.SdcLogger;
import mdibLoading.MdibFileReader;
import move.imd.MoveExtensionConfig;
import sdclib.ApsProvicerFactory;
import sdclib.ApsSdcProvider;

public class BaseProviderTest {
	
	public static final int SUBSCRIPTION_DELAY = 4000;
	public static final int DISCOVERY_DELAY = 10000;
	
	public static final String DUT_H_ACT_OP = "handle_activate";
	
	public static final String expectedProviderBackboneEPR = "urn:uuid:Backbone";
	public static final String expectedProviderDutEPR = "urn:uuid:DUT";

	static ApsSdcProvider providerBB;
	static ApsSdcProvider providerDut;

	static SDCConsumer consumerDut = null;
	static SDCConsumer consumerBB = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		System.err.println("\n\n\n");
		System.err.println("###########################");
		System.err.println("### START of Test Class ###");
		System.err.println("###########################");
		System.err.println("\n\n\n");
		
		Random rand = new Random(System.currentTimeMillis());
		
		int randomPortOffset = rand.nextInt(1000);
				
		// Read config file
		if ( !Config.getInstance().readXmlConfig("src/test/resources/test_config.xml") ) {
			return;
		}
		
		MoveExtensionConfig.getInstance().setPlainMessageDistributionTcpServerPort(MoveExtensionConfig.getInstance().getPlainMessageDistributionTcpServerPort() + randomPortOffset);
		Config.getInstance().setBackboneInterfaceStartPort(Config.getInstance().getBackboneInterfaceStartPort() + randomPortOffset*10);
		Config.getInstance().setDutInterfaceStartPort(Config.getInstance().getDutInterfaceStartPort() + randomPortOffset*10);


		//try to read the MDIB file
		MdibFileReader mfr = new MdibFileReader();
		String mdibContent = mfr.readMdibFromXmlFile("src/test/resources/test_device_description.xml");
		
		if(mdibContent == null) {
			return;
		}

		// start the SDCLib/J framework
		SdcLogger.log.info("Starting SDCLib/J...");
		SDCLib.getInstance().setLogLevel(Level.CONFIG);
		SDCLib.getInstance().startup();
		SDCLib.getInstance().setSchemaValidationEnabled(true);
		
		MDPWSTransportLayerDetail configuration = SDCLib.getInstance().getDefaultTransportLayerConfig(MDPWSTransportLayerConfiguration.class).getConfigurationDetail();
        configuration.setPortStart(30000); 

		providerBB = new ApsSdcProvider(true);
		providerDut = new ApsSdcProvider(false);

//		ApsProvicerFactory.initMirroredProviders(providerDut, providerBB, mdibContent, "139.30.201.95", "192.168.100.210", providerDutEPR, providerBackboneEPR);
//		ApsProvicerFactory.initMirroredProviders(providerDut, providerBB, mdibContent, "192.168.56.1", "192.168.100.210", providerDutEPR, providerBackboneEPR);
		ApsProvicerFactory.initMirroredProviders(providerDut, providerBB, mdibContent, Config.getInstance().getDutInterfaceEpr(), Config.getInstance().getBackboneInterfaceEpr());

		// attention in ANY CASE: start BB before DUT!
		providerBB.startup();
		providerDut.startup();
		

		try {
			Thread.sleep(SUBSCRIPTION_DELAY);
		} catch (InterruptedException ex) {
			Logger.getLogger(MirrorProviderTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.err.println("Start searching...");
		SyncEvent eventDutDiscovery = new SyncEvent();
		SDCLib.getInstance().getDefaultServiceManager().discoverEPRAsync(expectedProviderDutEPR, (c) -> {
            // Consumer found
            // Note: Normally, avoid event-based waiting. Call a suitable method from here.
			// Note: copied from example code
            consumerDut = c;
            eventDutDiscovery.set();
        });
        // Wait until discovered
        eventDutDiscovery.wait(DISCOVERY_DELAY);
        
        consumerDut.addLifecycleHandler(new mySDCLifecycleHandler());
        
		SyncEvent eventBbDiscovery = new SyncEvent();
		SDCLib.getInstance().getDefaultServiceManager().discoverEPRAsync(expectedProviderBackboneEPR, (c) -> {
            // Consumer found
            // Note: Normally, avoid event-based waiting. Call a suitable method from here.
			// Note: copied from example code
			consumerBB = c;
            eventDutDiscovery.set();
        });
        // Wait until discovered
        eventBbDiscovery.wait(DISCOVERY_DELAY);
		
        consumerBB.addLifecycleHandler(new mySDCLifecycleHandler());
	}

	protected static class mySDCLifecycleHandler extends SDCLifecycleHandler {
        @Override
        public void onConnectionLost(SDCConsumer consumer) {
            System.out.println("Connection lost!");
        }

        @Override
        public void onConnectionReestablished(SDCConsumer consumer) {
            System.out.println("Connection reestablished!");
        }

        @Override
        public void onClosed(SDCConsumer consumer) {
            System.out.println("Connection closed!");
        }

        @Override
        public void onOpened(SDCConsumer consumer) {
            System.out.println("Connection opened!");
        }
    };

	@AfterClass
	public static void tearDownAfterClass() throws Exception {	
//	FIXME ask Andreas why the close and shutdown methods are no longer available
		if (consumerDut != null) {
			consumerDut.clearEventHandlers();
			consumerDut = null;
		}
		if (consumerBB != null) {
			consumerBB.clearEventHandlers();
			consumerDut = null;
		}
		
		providerDut.close();
		providerBB.close();
		
		SDCLib.getInstance().shutdown();
		
		// waiting to be sure that the subscription was really successful 
		try {
			Thread.sleep(7500);
		} catch (InterruptedException ex) {
			Logger.getLogger(MirrorProviderTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		SDCLib.destroyInstance();
		
		System.err.println("\n\n\n");
		System.err.println("#########################");
		System.err.println("### END of Test Class ###");
		System.err.println("#########################");
		System.err.println("\n\n\n");
	}

	@Before
	public void setUp() throws Exception {
		System.err.println("Start 'Before'...");
		assertEquals(providerDut.isRunning(), true);
		assertEquals(providerBB.isRunning(), true);
		assertNotNull(consumerDut);
		if (consumerDut == null)
			return;
		assertNotNull(consumerBB);
		if (consumerBB == null)
			return;
		
		System.err.println("... end 'Before'");
	}

	@After
	public void tearDown() throws Exception {
	}


}
