package apsTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;
import org.ornet.cdm.EnumStringMetricState;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.StringMetricState;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.common.SyncEvent;
import org.ornet.sdclib.consumer.FutureInvocationState;
import org.ornet.sdclib.consumer.SDCConsumerEventHandler;
import org.ornet.sdclib.provider.OperationInvocationContext;

import util.Constants;

public class DutProviderLifeCycleTest extends BaseProviderTest {
	
	@Test
	public void testShutdownAndStartUpOfDutInterfaceViaBB() {
		List<String> bbConfigHandles = new ArrayList<String>();
		String bbConfigMetricHandle = Constants.HANDLE_CONFIG_PROVIDER_OPERATION_STATE_METRIC;
		bbConfigHandles.add(bbConfigMetricHandle);
		
		EnumStringMetricState bbConfigState = (EnumStringMetricState) consumerBB.getMDState(bbConfigHandles).getState().get(0);
		//initially we expect the the DUT is running (we already know from pretext condition) and expect that the metric has a corresponding value
		assertEquals(bbConfigState.getMetricValue().getValue(), Constants.DUT_PROVIDER_OPERATION_STATE_RUNNING);
		
		consumerDut.clearEventHandlers();
		consumerDut = null;
		
		// implement event handler
		final AtomicBoolean changeEventReceivedBbConfig = new AtomicBoolean(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<StringMetricState>(bbConfigMetricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [Backbone] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(StringMetricState state) {
                changeEventReceivedBbConfig.set(true);
            }
        });
		
		// now, let's manipulate the Backbone interface
		FutureInvocationState fis = new FutureInvocationState();
		String bbConfigTestString = Constants.DUT_PROVIDER_OPERATION_STATE_NOT_RUNNING;
		bbConfigState.getMetricValue().setValue(bbConfigTestString);
        assertEquals(InvocationState.WAIT, consumerBB.commitState(bbConfigState, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FAIL, 10000)); //please note - FAIL is intended... :-/
        
        // let search for the DUT -> expected: not DUT available 
        System.err.println("### Start searching...");
		SyncEvent eventDutDiscovery = new SyncEvent();
		SDCLib.getInstance().getDefaultServiceManager().discoverEPRAsync(expectedProviderDutEPR, (c) -> {
            // Consumer found
            // Note: Normally, avoid event-based waiting. Call a suitable method from here.
			// Note: copied from example code
            consumerDut = c;
            eventDutDiscovery.set();
        });
        // Wait until discovered
        eventDutDiscovery.wait(DISCOVERY_DELAY);
        
        assertNull(consumerDut);
        
        // we expect the config metric to be set to not running
        bbConfigState = (EnumStringMetricState) consumerBB.getMDState(bbConfigHandles).getState().get(0);
		assertEquals(bbConfigState.getMetricValue().getValue(), Constants.DUT_PROVIDER_OPERATION_STATE_NOT_RUNNING);
		
		// let's manipulate a metric at BB 
		List<String> stringMetricHandles = new ArrayList<String>();
		String metricHandle = "string_met";
		stringMetricHandles.add(metricHandle);
		
		StringMetricState smsBB = (StringMetricState) consumerBB.getMDState(stringMetricHandles).getState().get(0);
		
		// implement event handler
		final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<StringMetricState>(metricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(StringMetricState state) {
                changeEventReceived.set(true);
            }
        });
		
		// now, let's manipulate the Backbone interface
		fis = new FutureInvocationState();
		String testString = "test value";
		for (int i = 0; i < 5; i++) {
			testString = testString + " " + i;
	        smsBB.getMetricValue().setValue(testString);
	        assertEquals(InvocationState.WAIT, consumerBB.commitState(smsBB, fis));
	        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
	        
	        // and get the value again to check whether the new value is available
	        smsBB = (StringMetricState) consumerBB.getMDState(stringMetricHandles).getState().get(0);
	        assertEquals(true, smsBB.getMetricValue().getValue().equals(testString));
		}
		
		List<String> numiercMetricHandles = new ArrayList<String>();
		String numericMetricHandle = "num_met";
		numiercMetricHandles.add(numericMetricHandle);
		
		NumericMetricState nmsBB = (NumericMetricState) consumerBB.getMDState(numiercMetricHandles).getState().get(0);
		
		// implement event handler
		final AtomicBoolean changeEventReceivedNumeric = new AtomicBoolean(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<NumericMetricState>(numericMetricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [Backbone - " + numericMetricHandle + "] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                changeEventReceivedNumeric.set(true);
            }
        });
		
		// now, let's manipulate the Backbone interface
		fis = new FutureInvocationState();

        nmsBB.getMetricValue().setValue(BigDecimal.valueOf(42));
        assertEquals(InvocationState.WAIT, consumerBB.commitState(nmsBB, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
        
        // and get the value again to check whether the new value is available
        nmsBB = (NumericMetricState) consumerBB.getMDState(numiercMetricHandles).getState().get(0);
        assertEquals(true, nmsBB.getMetricValue().getValue().equals(BigDecimal.valueOf(42)));

        
        
		// turn on again
		fis = new FutureInvocationState();
		bbConfigTestString = Constants.DUT_PROVIDER_OPERATION_STATE_RUNNING;
		bbConfigState.getMetricValue().setValue(bbConfigTestString);
        assertEquals(InvocationState.WAIT, consumerBB.commitState(bbConfigState, fis));
        fis.waitReceived(InvocationState.FAIL, 20000); //please note - FAIL is intended... :-/
		
        // we expect the config metric to be set to running
        bbConfigState = (EnumStringMetricState) consumerBB.getMDState(bbConfigHandles).getState().get(0);
		assertEquals(bbConfigState.getMetricValue().getValue(), Constants.DUT_PROVIDER_OPERATION_STATE_RUNNING);
		
		// let search for the DUT -> expected: DUT is available 
		for ( int i = 0; i < 5; i++ ) {
	        System.err.println("[After Restart]  Start searching...");
	        final AtomicBoolean found = new AtomicBoolean(false);
	        SyncEvent eventDutDiscoveryRetry = new SyncEvent();
			SDCLib.getInstance().getDefaultServiceManager().discoverEPRAsync(expectedProviderDutEPR, (c) -> {
	            // Consumer found
				System.err.println("[After Restart] Found DUT Provider...");
	            consumerDut = c;
	            eventDutDiscoveryRetry.set();
	            found.set(true);
	        });
	        // Wait until discovered
	        eventDutDiscoveryRetry.wait(DISCOVERY_DELAY);
        
	        if (  found.get() ) {
	        	break;
	        }
		}
        
        assertNotNull(consumerDut);
		
        consumerDut.addLifecycleHandler(new mySDCLifecycleHandler());
        
		
        // test: changed multiple times
        StringMetricState smsDutAfterRestart = (StringMetricState) consumerDut.getMDState(stringMetricHandles).getState().get(0);
		StringMetricState smsBBAfterRestart = (StringMetricState) consumerBB.getMDState(stringMetricHandles).getState().get(0);
		
		assertEquals(testString, smsBBAfterRestart.getMetricValue().getValue());
		assertEquals(testString, smsDutAfterRestart.getMetricValue().getValue());
		assertEquals(smsBBAfterRestart.getStateVersion(), smsDutAfterRestart.getStateVersion());
		
		// test: changed once
		NumericMetricState nmsBBAfertRestart = (NumericMetricState) consumerBB.getMDState(numiercMetricHandles).getState().get(0);
		NumericMetricState nmsDutAfertRestart = (NumericMetricState) consumerDut.getMDState(numiercMetricHandles).getState().get(0);

		assertEquals(true, nmsDutAfertRestart.getMetricValue().getValue().equals(BigDecimal.valueOf(42)));
		assertEquals(true, nmsBBAfertRestart.getMetricValue().getValue().equals(BigDecimal.valueOf(42)));
		assertEquals(nmsBBAfertRestart.getStateVersion(), nmsDutAfertRestart.getStateVersion());
		
		// test: no changes
		List<String> stringMetricHandles2 = new ArrayList<String>();
		String metricHandle2 = "string_met_2";
		stringMetricHandles2.add(metricHandle2);
		
		StringMetricState smsDutAfterRestart2 = (StringMetricState) consumerDut.getMDState(stringMetricHandles2).getState().get(0);
		StringMetricState smsBBAfterRestart2 = (StringMetricState) consumerBB.getMDState(stringMetricHandles2).getState().get(0);
		
		assertEquals(true, smsBBAfterRestart2.getMetricValue().getValue().equals(smsDutAfterRestart2.getMetricValue().getValue()));
		assertEquals(smsBBAfterRestart2.getStateVersion(), smsDutAfterRestart2.getStateVersion());
		
	}

}
