package apsTests;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Test;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.AbstractMetricDescriptor.Relation;
import org.ornet.cdm.AbstractOperationDescriptor;
import org.ornet.cdm.AbstractOperationState;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertSignalDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.EnumStringMetricState;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdState;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.ScoDescriptor;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.sdclib.SDCLib;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.common.SyncEvent;
import org.ornet.sdclib.consumer.FutureInvocationState;
import org.ornet.sdclib.consumer.SDCConsumer;
import org.ornet.sdclib.consumer.SDCConsumerEventHandler;
import org.ornet.sdclib.consumer.SDCConsumerOperationInvokedHandler;
import org.ornet.sdclib.provider.OperationInvocationContext;

import util.Constants;

/**
 * FIXME add test for value simulation 
 *
 */
public class MirrorProviderTest extends BaseProviderTest {
	


	

	@Test
	public void testGetMdibFromBothProviders() {
		long startTime = System.currentTimeMillis();
		assertEquals(true, consumerBB.getMDIB() != null);
		assertEquals(true, consumerDut.getMDIB() != null);
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Time Get MDIB: " + elapsedTime);
	}
	
	@Test
	public void testCheckUniquenessOfHandles() {

		assertEquals(true, consumerBB.getMDIB() != null);
		assertEquals(true, consumerDut.getMDIB() != null);

		assertTrue(checkUniquenessOfHandlesOfAnMdDescription(consumerBB.getMDIB().getMdDescription()));
		assertTrue(checkUniquenessOfHandlesOfAnMdDescription(consumerDut.getMDIB().getMdDescription()));
	}
	
	/**
	 * FIXME add test for missing object that have handles, e.g. Clock, Battery etc.
	 * @param mdd
	 * @return
	 */
	private boolean checkUniquenessOfHandlesOfAnMdDescription(MdDescription mdd) {
		boolean ret = true;
		List<String> handleList = new ArrayList<>();
		
		for ( MdsDescriptor mdsDescriptor : mdd.getMds() ) {
			if ( ! checkUniquenessOfNewElement(mdsDescriptor.getHandle(), handleList) ) {
				ret = false;
			}
			if ( mdsDescriptor.getSco() != null ) {
				for (AbstractOperationDescriptor aod : mdsDescriptor.getSco().getOperation()) {
					if ( ! checkUniquenessOfNewElement(aod.getHandle(), handleList) ) {
						ret = false;
					}
				}
			}
			if ( mdsDescriptor.getAlertSystem() != null ) {
				for ( AlertConditionDescriptor acd : mdsDescriptor.getAlertSystem().getAlertCondition() ) {
					if ( ! checkUniquenessOfNewElement(acd.getHandle(), handleList) ) {
						ret = false;
					}
				}
				for ( AlertSignalDescriptor asd : mdsDescriptor.getAlertSystem().getAlertSignal()) {
					if ( ! checkUniquenessOfNewElement(asd.getHandle(), handleList) ) {
						ret = false;
					}
				}
			}
			for ( VmdDescriptor vmdDescriptor : mdsDescriptor.getVmd() ) {
				if ( ! checkUniquenessOfNewElement(vmdDescriptor.getHandle(), handleList) ) {
					ret = false;
				}
				if ( vmdDescriptor.getSco() != null ) {
					for (AbstractOperationDescriptor aod : mdsDescriptor.getSco().getOperation()) {
						if ( ! checkUniquenessOfNewElement(aod.getHandle(), handleList) ) {
							ret = false;
						}
					}
				}
				if ( vmdDescriptor.getAlertSystem() != null ) {
					for ( AlertConditionDescriptor acd : vmdDescriptor.getAlertSystem().getAlertCondition() ) {
						if ( ! checkUniquenessOfNewElement(acd.getHandle(), handleList) ) {
							ret = false;
						}
					}
					for ( AlertSignalDescriptor asd : vmdDescriptor.getAlertSystem().getAlertSignal()) {
						if ( ! checkUniquenessOfNewElement(asd.getHandle(), handleList) ) {
							ret = false;
						}
					}
				}
				for ( ChannelDescriptor cd : vmdDescriptor.getChannel() ) {
					if ( ! checkUniquenessOfNewElement(cd.getHandle(), handleList) ) {
						ret = false;
					}
					for ( AbstractMetricDescriptor amd : cd.getMetric() ) {
						if ( ! checkUniquenessOfNewElement(amd.getHandle(), handleList) ) {
							ret = false;
						}
					}
				}
			}
		}
		
		return ret;
	}
	
	private boolean checkUniquenessOfNewElement(String newElement, List<String> list) {
		if ( list.contains(newElement) ) {
			System.err.println("[ERROR] Following String is NOT unique: " + newElement);
			return false;
		} 
		else {
			list.add(newElement);
			return true;
		}

	}

	@Test
	public void testMdDescription() {
		assertEquals(true, consumerBB.getMDIB() != null);
		assertEquals(true, consumerDut.getMDIB() != null);
		
		assertNotNull(SDCToolbox.findMetricState(consumerBB, "string_met"));
		assertNotNull(SDCToolbox.findMetricState(consumerDut, "string_met"));
		assertNotNull(SDCToolbox.findMetricState(consumerBB, "num_met"));
		assertNotNull(SDCToolbox.findMetricState(consumerDut, "num_met"));

		

		assertNotNull(SDCToolbox.findState(consumerBB, "BB_SCO_string_met", AbstractOperationState.class));
		assertNull(SDCToolbox.findState(consumerDut, "BB_SCO_string_met", AbstractOperationState.class));

		assertNotNull(SDCToolbox.findState(consumerBB, "BB_SCO_num_met_sco_0", AbstractOperationState.class));
		assertNotNull(SDCToolbox.findState(consumerBB, "num_met_sco_0", AbstractOperationState.class));
		assertNotNull(SDCToolbox.findState(consumerDut, "num_met_sco_0", AbstractOperationState.class));

	}
	
	@Test
	public void testManipulateFromBackbone() {
		List<String> handles = new ArrayList<String>();
		String metricHandle = "string_met";
		handles.add(metricHandle);
		
		StringMetricState smsDut = (StringMetricState) consumerDut.getMDState(handles).getState().get(0);
		StringMetricState smsBB = (StringMetricState) consumerBB.getMDState(handles).getState().get(0);
		
		System.err.println("+++++++ BB: " + smsBB.getMetricValue().getValue() + " " + smsBB.getStateVersion());
		System.err.println("+++++++ Dut: " + smsDut.getMetricValue().getValue() + " " + smsDut.getStateVersion());
		
		assertEquals(true, smsBB.getMetricValue().getValue().equals(smsDut.getMetricValue().getValue()));
		assertEquals(smsBB.getStateVersion(), smsDut.getStateVersion());
		
		// implement event handler
		final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<StringMetricState>(metricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [Backbone - testManipulateFromBackbone()] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(StringMetricState state) {
                changeEventReceived.set(true);
            }
        });
		
		// now, let's manipulate the Backbone interface
		FutureInvocationState fis = new FutureInvocationState();
		String testString = "test value";
        smsBB.getMetricValue().setValue(testString);
        assertEquals(InvocationState.WAIT, consumerBB.commitState(smsBB, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, 10000));
        
        // and get the value again to check whether the new value is available
        smsBB = (StringMetricState) consumerBB.getMDState(handles).getState().get(0);
        smsDut = (StringMetricState) consumerDut.getMDState(handles).getState().get(0);
        assertEquals(true, smsBB.getMetricValue().getValue().equals(testString));
        assertEquals(true, smsDut.getMetricValue().getValue().equals(testString));
        assertEquals(smsBB.getStateVersion(), smsDut.getStateVersion());		
	}
	
	@Test
	public void testManipulateFromDUT() {
		List<String> handles = new ArrayList<String>();
		String metricHandle = "num_met";
		handles.add(metricHandle);
		
		NumericMetricState nmsDut = (NumericMetricState) consumerDut.getMDState(handles).getState().get(0);
		NumericMetricState nmsBB = (NumericMetricState) consumerBB.getMDState(handles).getState().get(0);
		
		assertEquals(true, nmsBB.getMetricValue().getValue().equals(nmsDut.getMetricValue().getValue()));
		assertEquals(nmsBB.getStateVersion(), nmsDut.getStateVersion());
		
		// implement event handler
		final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        consumerDut.addEventHandler(new SDCConsumerEventHandler<NumericMetricState>(metricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [DUT] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                changeEventReceived.set(true);
            }
        });
		
		// now, let's manipulate from the DUT interface
		FutureInvocationState fis = new FutureInvocationState();
		BigDecimal testValue = BigDecimal.valueOf(42);
        nmsDut.getMetricValue().setValue(testValue);
        assertEquals(InvocationState.WAIT, consumerDut.commitState(nmsDut, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, SUBSCRIPTION_DELAY));
        
        // and get the value again to check whether the new value is available
        nmsBB = (NumericMetricState) consumerBB.getMDState(handles).getState().get(0);
        nmsDut = (NumericMetricState) consumerDut.getMDState(handles).getState().get(0);
        
        assertEquals(true, nmsBB.getMetricValue().getValue().equals(testValue));
        assertEquals(true, nmsDut.getMetricValue().getValue().equals(testValue));
        assertEquals(nmsBB.getStateVersion(), nmsDut.getStateVersion());        	
	}
	
	
	
	@Test
	/**
	 * @note due to implementation, the change of the metrics configuring the manipulation will return FAIL in any case
	 */
	public void testSingleStateVersionManipulation() {
		List<String> handles = new ArrayList<String>();
		String metricHandle = "string_met_2";
		handles.add(metricHandle);
		
		StringMetricState smsDut = (StringMetricState) consumerDut.getMDState(handles).getState().get(0);
		StringMetricState smsBB = (StringMetricState) consumerBB.getMDState(handles).getState().get(0);
		
		assertEquals(true, smsBB.getMetricValue().getValue().equals(smsDut.getMetricValue().getValue()));
		assertEquals(smsBB.getStateVersion(), smsDut.getStateVersion());
		
		BigInteger initialStateVersion = smsBB.getStateVersion();
		
		// implement event handler
		final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<StringMetricState>(metricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [Backbone] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(StringMetricState state) {
                changeEventReceived.set(true);
            }
        });
		
		// now, let's manipulate the Backbone interface
		FutureInvocationState fis = new FutureInvocationState();
		String testString = "testSingleStateVersionManipulation";
        smsBB.getMetricValue().setValue(testString);
        assertEquals(InvocationState.WAIT, consumerBB.commitState(smsBB, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
        
        // and get the value again to check whether the new value is available
        smsBB = (StringMetricState) consumerBB.getMDState(handles).getState().get(0);
        smsDut = (StringMetricState) consumerDut.getMDState(handles).getState().get(0);
        assertEquals(true, smsBB.getMetricValue().getValue().equals(testString));
        assertEquals(true, smsDut.getMetricValue().getValue().equals(testString));
        assertEquals(smsBB.getStateVersion(), smsDut.getStateVersion());
        assertEquals(initialStateVersion.add(BigInteger.ONE), smsBB.getStateVersion());
        assertNotEquals(BigInteger.ZERO, smsBB.getStateVersion());
        
        // now, let's turn on the state manipulation -> first set the state version
        List<String> handlesSV = new ArrayList<String>();
		String metricHandleSV = Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_STATE_VERSION;
		handlesSV.add(metricHandleSV);

		StringMetricState smsBBStateVersion = (StringMetricState) consumerBB.getMDState(handlesSV).getState().get(0);
		// implement event handler
		changeEventReceived.set(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<StringMetricState>(
        		Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_STATE_VERSION) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [Backbone] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(StringMetricState state) {
                changeEventReceived.set(true);
            }
        });
		
		// now, let's manipulate the Backbone interface
		fis = new FutureInvocationState();
        smsBBStateVersion.getMetricValue().setValue("0");
        assertEquals(InvocationState.WAIT, consumerBB.commitState(smsBBStateVersion, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FAIL, 2000));       
        
        // now, let's turn on the state manipulation -> second set the state version
        List<String> handlesDH = new ArrayList<String>();
		String metricHandleDH = Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_DESC_HANDLE;
		handlesDH.add(metricHandleDH);

		StringMetricState smsBBDescrHandle = (StringMetricState) consumerBB.getMDState(handlesDH).getState().get(0);
		// implement event handler
		changeEventReceived.set(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<StringMetricState>(
        		Constants.HANDLE_MSG_MANIPULATION_SINGLE_STATE_METRIC_DESC_HANDLE) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [Backbone] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(StringMetricState state) {
                changeEventReceived.set(true);
            }
        });
		
		// now, let's manipulate the Backbone interface
		fis = new FutureInvocationState();
        smsBBDescrHandle.getMetricValue().setValue(metricHandle);
        assertEquals(InvocationState.WAIT, consumerBB.commitState(smsBBDescrHandle, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FAIL, 2000));       
        
        
        // and get the value again to check whether the new value is available
        smsBB = (StringMetricState) consumerBB.getMDState(handles).getState().get(0);
        smsDut = (StringMetricState) consumerDut.getMDState(handles).getState().get(0);
        assertEquals(true, smsBB.getMetricValue().getValue().equals(testString));
        assertEquals(true, smsDut.getMetricValue().getValue().equals(testString));
        assertEquals(smsBB.getStateVersion(), smsDut.getStateVersion());
        assertEquals(BigInteger.ZERO, smsBB.getStateVersion());
        
        //and turn manipulation off again
        changeEventReceived.set(false);
        // now, let's manipulate the Backbone interface
     	fis = new FutureInvocationState();
        smsBBDescrHandle.getMetricValue().setValue("");
        assertEquals(InvocationState.WAIT, consumerBB.commitState(smsBBDescrHandle, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FAIL, 2000));    
        
        // and get the value again to check whether the new value is available
        smsBB = (StringMetricState) consumerBB.getMDState(handles).getState().get(0);
        smsDut = (StringMetricState) consumerDut.getMDState(handles).getState().get(0);
        assertEquals(true, smsBB.getMetricValue().getValue().equals(testString));
        assertEquals(true, smsDut.getMetricValue().getValue().equals(testString));
        assertEquals(smsBB.getStateVersion(), smsDut.getStateVersion());
        assertEquals(initialStateVersion.add(BigInteger.ONE), smsBB.getStateVersion());
        assertNotEquals(BigInteger.ZERO, smsBB.getStateVersion());
	}
	
	private boolean lookForOperation(SDCConsumer sdcConsumer, String operationHandle) {
		MdsDescriptor mdsDesc = sdcConsumer.getMDDescription().getMds().get(0);
		assertNotNull(mdsDesc);
		ScoDescriptor scoDesc = mdsDesc.getSco();
		assertNotNull(scoDesc);
		boolean foundOperation = false;
		for ( AbstractOperationDescriptor aod : scoDesc.getOperation() ) {
			if ( aod.getHandle().equals(operationHandle) ) {
				foundOperation = true;
				break;
			}
		}
		assertTrue(foundOperation);
		
		return foundOperation;
	}
	
	private boolean lookForMetric(SDCConsumer sdcConsumer, String metricCode, String operationHandle) {
		MdsDescriptor mdsDesc = sdcConsumer.getMDDescription().getMds().get(0);
		assertNotNull(mdsDesc);

		boolean foundMetric = false;
		for ( VmdDescriptor vmd : mdsDesc.getVmd() ) {
			for ( ChannelDescriptor ch : vmd.getChannel() ) {
				for ( AbstractMetricDescriptor metric : ch.getMetric() ) {
					if ( metric.getType().getCode().equals(metricCode) ) {
						for (Relation relationRef : metric.getRelation()) {
							for (String entry : relationRef.getEntries() ) {
								if ( entry.equals(operationHandle) ) {
									foundMetric = true;
									break;
								}
							}
						}
					}
				}
			}
		}
		
		return foundMetric;
	}
	
	
	@Test
	public void testActivateOperationMetricMappingTest() {
		String opHandle = "handle_activate";
		
		// check whether the config VMD and channel exist
		boolean foundConfigVmd = false;
		boolean foundConfigChannel = false;
		for (MdsDescriptor mdsDesc : consumerBB.getMDDescription().getMds() ) {
			for ( VmdDescriptor vmdDescr : mdsDesc.getVmd() ) {
				if ( vmdDescr.getHandle().equals("h_bb_vmd") 
						&& vmdDescr.getType().getCode().equals("MDCX_VMD_BB") 
						) {
					foundConfigVmd = true;
					for ( ChannelDescriptor channel : vmdDescr.getChannel() ) {
						if ( channel.getHandle().equals("h_bb_act_opt_channel")) {
							foundConfigChannel = true;
						}
					}
				}
			}
		}
		
		assertTrue(foundConfigVmd);
		assertTrue(foundConfigChannel);
		
		
		// look whether there is an activate operation on both consumers
		assertTrue(lookForOperation(consumerDut, opHandle));
		assertTrue(lookForOperation(consumerBB, opHandle));
		
		// look for the "counting metric" representing the activation operation
		assertTrue(lookForMetric(consumerBB, Constants.CODE_ACT_OP_COUNT_METRIC, opHandle)); // shall be available at BB
		assertFalse(lookForMetric(consumerDut, Constants.CODE_ACT_OP_COUNT_METRIC, opHandle)); // shall NOT be available at DUT
	}
	
    @Test
    public void testActivateOperationDUT() {
    	List<String> handles = new ArrayList<String>();
    	handles.add(Constants.PREFIX_HANDLE_ACT_OP_COUNTER + DUT_H_ACT_OP);
    	
    	assertEquals(true, consumerBB.getMDIB() != null);
    	MdState mdState = consumerBB.getMDState();
    	assertNotNull(mdState);
    	
    	// get initial value
    	NumericMetricState nmsBB = (NumericMetricState) consumerBB.requestState(handles.get(0), NumericMetricState.class);
    	assertNotNull(nmsBB);
    	BigDecimal initialCounterValue = nmsBB.getMetricValue().getValue();    	
    	
    	// implement event handler for DUT interface
        final AtomicBoolean invokeEventReceived = new AtomicBoolean(false);
        consumerDut.addEventHandler(new SDCConsumerOperationInvokedHandler(DUT_H_ACT_OP) {
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [DUT Interface] Activate Operation Command received from handle = " + getDescriptorHandle() ); 
                invokeEventReceived.set(true);
            }
        });
        
		// implement event handler for BB interface
		final AtomicBoolean changeEventReceivedForCounterMetric = new AtomicBoolean(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<NumericMetricState>(Constants.PREFIX_HANDLE_ACT_OP_COUNTER + DUT_H_ACT_OP) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [BB Interface] Received Metric change 'Activate Operation Counter' with handle = " + getDescriptorHandle());                
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                changeEventReceivedForCounterMetric.set(true);
            }
        });
        
        // waiting to be sure that the subscription was really successful 
		try {
			Thread.sleep(SUBSCRIPTION_DELAY);
		} catch (InterruptedException ex) {
			Logger.getLogger(MirrorProviderTest.class.getName()).log(Level.SEVERE, null, ex);
		}
        
        
        
        FutureInvocationState fis = new FutureInvocationState();
        assertEquals(InvocationState.WAIT, consumerDut.activate(DUT_H_ACT_OP, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, SUBSCRIPTION_DELAY));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(MirrorProviderTest.class.getName()).log(Level.SEVERE, null, ex);
        }                
        
        assertEquals(true, invokeEventReceived.get());
        assertEquals(true, changeEventReceivedForCounterMetric.get());
        
        // and get the value again from internal 
        nmsBB = (NumericMetricState) consumerBB.getMDState(handles).getState().get(0);
        BigDecimal newCounterValue = nmsBB.getMetricValue().getValue();
        
        BigDecimal assumedCounterValue = initialCounterValue.add(BigDecimal.ONE);
        boolean passed = assumedCounterValue.equals(newCounterValue);
        System.out.println("new value of counter metric: " + newCounterValue);
        assertTrue(passed);
    }


	
//	@Test
//	public void testManipulateFromDutWithLowerStateVersion() {
//		
//		
//		providerDut.useWrongStateVersion = true; //TODO
//		
//		List<String> handles = new ArrayList<String>();
//		String metricHandle = "num_met";
//		handles.add(metricHandle);
//		
//		NumericMetricState nmsDut = (NumericMetricState) consumerDut.getMDState(handles).getState().get(0);
//		NumericMetricState nmsBB = (NumericMetricState) consumerBB.getMDState(handles).getState().get(0);
//		
//		assertEquals(true, nmsBB.getMetricValue().getValue().equals(nmsDut.getMetricValue().getValue()));
//		assertEquals(nmsBB.getStateVersion(), nmsDut.getStateVersion());
//		
//		BigInteger currentStateVersion = nmsDut.getStateVersion();
//		
//		// implement event handler
//		final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
//        consumerDut.addEventHandler(new SDCConsumerEventHandler<NumericMetricState>(metricHandle) {
//            
//            @Override
//            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
//                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
//            }
//            
//            @Override
//            public void onStateChanged(NumericMetricState state) {
//                changeEventReceived.set(true);
//            }
//        });
//		
//		// now, let's manipulate from the DUT interface
//		FutureInvocationState fis = new FutureInvocationState();
//		BigDecimal testValue = BigDecimal.valueOf(42);
//        nmsDut.getMetricValue().setValue(testValue);
//        assertEquals(InvocationState.WAIT, consumerDut.commitState(nmsDut, fis));
//        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
//        
//        // and get the value again to check whether the new value is available
//        nmsBB = (NumericMetricState) consumerBB.getMDState(handles).getState().get(0);
//        nmsDut = (NumericMetricState) consumerDut.getMDState(handles).getState().get(0);
//        
//        assertEquals(true, nmsBB.getMetricValue().getValue().equals(testValue));
//        assertEquals(true, nmsDut.getMetricValue().getValue().equals(testValue));
//        assertEquals(nmsBB.getStateVersion(), nmsDut.getStateVersion());
//        
//        BigInteger newStateVersion = nmsDut.getStateVersion();
//        
//        assertEquals(true, newStateVersion.compareTo(currentStateVersion) < 0 ); 
//        	
//	}

	
	@Test
	public void testManipulateFromInternal() {
		List<String> handles = new ArrayList<String>();
		String metricHandle = "num_met";
		handles.add(metricHandle);
		
		NumericMetricState nmsDut = (NumericMetricState) consumerDut.getMDState(handles).getState().get(0);
		NumericMetricState nmsBB = (NumericMetricState) consumerBB.getMDState(handles).getState().get(0);
		
		assertEquals(true, nmsBB.getMetricValue().getValue().equals(nmsDut.getMetricValue().getValue()));
		assertEquals(nmsBB.getStateVersion(), nmsDut.getStateVersion());
		
		// implement event handler for DUT interface
		final AtomicBoolean changeEventReceivedDut = new AtomicBoolean(false);
        consumerDut.addEventHandler(new SDCConsumerEventHandler<NumericMetricState>(metricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [DUT] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                changeEventReceivedDut.set(true);
            }
        });
        
        // implement event handler for Backbone interface
        final AtomicBoolean changeEventReceivedBB = new AtomicBoolean(false);
        consumerBB.addEventHandler(new SDCConsumerEventHandler<NumericMetricState>(metricHandle) {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.err.println("   [Backbone] Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                changeEventReceivedBB.set(true);
            }
        });
        
        // waiting to be sure that the subscription was really successful 
		try {
			Thread.sleep(SUBSCRIPTION_DELAY);
		} catch (InterruptedException ex) {
			Logger.getLogger(MirrorProviderTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		// now, let's manipulate value internally
		BigDecimal testValue = BigDecimal.valueOf(7411);
		
		nmsBB.getMetricValue().setValue(testValue);
		
		providerBB.updateState(nmsBB);
		
        try {
			Thread.sleep(SUBSCRIPTION_DELAY / 2);
		} catch (InterruptedException ex) {
			Logger.getLogger(MirrorProviderTest.class.getName()).log(Level.SEVERE, null, ex);
		} 
		       
        // and get the value again to check whether the new value is available
        nmsBB = (NumericMetricState) consumerBB.getMDState(handles).getState().get(0);
        nmsDut = (NumericMetricState) consumerDut.getMDState(handles).getState().get(0);
        
        assertEquals(true, nmsBB.getMetricValue().getValue().equals(testValue));
        assertEquals(true, nmsDut.getMetricValue().getValue().equals(testValue));
        assertEquals(nmsBB.getStateVersion(), nmsDut.getStateVersion());
        
        try {
			Thread.sleep(SUBSCRIPTION_DELAY / 2);
		} catch (InterruptedException ex) {
			Logger.getLogger(MirrorProviderTest.class.getName()).log(Level.SEVERE, null, ex);
		}       
		
		assertEquals(true, changeEventReceivedBB.get());
		assertEquals(true, changeEventReceivedDut.get());
		
		
	}

	
//	@Test
//	public void alertSignalTestTriggerFromBB() {
//		List<String> handles = new ArrayList<String>();
//		String handleAlSig = "handle_alert_signal_latching";
//		handles.add(handleAlSig);
//		
//		MdsDescriptor mdsDescr = consumerBB.getMDDescription().getMds().get(0);
//		
//		List<Object> extensions = mdsDescr.getExtension().getAny();
//		
//		for( Object o : extensions) {
//			System.out.println(o.toString());
//		}
//		
//		
//		
//		AlertSignalState assDut = (AlertSignalState) consumerDut.getMDState(handles).getState().get(0);
//		AlertSignalState assBB = (AlertSignalState) consumerBB.getMDState(handles).getState().get(0);
//		
//		assertEquals(assBB.getStateVersion(), assDut.getStateVersion());
//		
//		// implement event handler
//		final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
//        consumerBB.addEventHandler(new SDCConsumerEventHandler<AlertSignalState>(handleAlSig) {
//            
//            @Override
//            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
//                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
//            }
//            
//            @Override
//            public void onStateChanged(AlertSignalState state) {
//            	System.out.println("AlertSignalState changed! New Presence: " + state.getPresence());
//                changeEventReceived.set(true);
//            }
//        });
//        
//        while(true) {
//        	try {
//				Thread.sleep(10000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//        	System.out.println(".");
//        }
//		
////		// now, let's manipulate the Backbone interface
////		FutureInvocationState fis = new FutureInvocationState();
////		String testString = "test value";
////        smsBB.getMetricValue().setValue(testString);
////        assertEquals(InvocationState.WAIT, consumerBB.commitState(smsBB, fis));
////        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
////        
////        // and get the value again to check whether the new value is available
////        smsBB = (StringMetricState) consumerBB.getMDState(handles).getState().get(0);
////        smsDut = (StringMetricState) consumerDut.getMDState(handles).getState().get(0);
////        assertEquals(true, smsBB.getMetricValue().getValue().equals(testString));
////        assertEquals(true, smsDut.getMetricValue().getValue().equals(testString));
////        assertEquals(smsBB.getStateVersion(), smsDut.getStateVersion());		
//	}
//



}
