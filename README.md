# Introduction
This tool is called "Abstract Provider Simulator (APS)". It is a generic simulator for IEEE 11073 Service-oriented Device Connectivity (SDC) service providers.  A brief introduction into SDC, including lots of links to papers, can be found at [Wikipedia]( https://en.wikipedia.org/wiki/IEEE_11073_service-oriented_device_connectivity). See also homepage of the [OR.NET](https://ornet.org/en/) association.

The APS has been built in the research project "MoVE - Modular Validation Environment for Medical Device Networks" funded by the German Federal Ministry of Education and Research (BMBF). 

As the roots of the APS are in the MoVE project, it is intended to work as part of the MoVE test platform as well as a stand-alone tool. Currently, the provided documentation only covers the stand-alone simulator functionalities. 

The APS is based on the [SDCLib/J](https://bitbucket.org/besting-it/sdclibcontrib/) communication library implementing IEEE 11073 SDC. Thus, I would like to thank and honor Andreas Besting for his great work and support.


# Build and Run
## Build
Attention: Java 11 or higher is required, due to requirements of SDCLib/J.

Go to the cloned directory first.

Building the project with:

```
mvn clean package 
```

You will find the executable files in the folder 'target'

## Executing
There are several ways to run the APS. Two options are explained below.

### Java
Navigate to the folder that contains the executable. (Typically, this is the folder 'target'.)

```
java -jar AbstractProviderSimulator-<VERSION>-jar-with-dependencies.jar <path-to-device-description-file> <path-to-config-file>
```
VERSION might be something like '9.2.0c-snapshot02'. Please make sure that you run the jar-file with dependencies.

Example:

```
cd target
java -jar AbstractProviderSimulator-9.2.0c-snapshot02-jar-with-dependencies.jar ..\example_descriptions\Descriptions\reference_mdib_mk-v01.xml ..\config.xml
```

#### Maybe not necessary anymore?
When running on linux using openjdk-11 +, make sure you have installed openjfx `sudo apt install openjfx`

```
java --module-path /usr/share/openjfx/lib --add-modules=javafx.fxml,javafx.base,javafx.graphics -jar AbstractProviderSimulator-SNAPSHOT-jar-with-dependencies.jar
```


### Maven
Navigate to the project root folder that contains the 'pom.xml' file.

```
mvn exec:java -Dexec.args="<path-to-device-description-file> <path-to-config-file>"
```

Example:

```
mvn exec:java -Dexec.args="example_descriptions\Descriptions\reference_mdib_mk-v01.xml config.xml"
```

# Config File
The tool will be started with a config file as input parameter. This config file specifies network parameters of the APS and some behavior parameters. An example file is provided in the root folder of the project: 'config.xml' 

In the 'config.xml' you can find a documentation of the parameters as XML comments.

**Attention:** Currently, the TLS support is a bit tricky as the project does not contain the certificate that has to be loaded. This will be fixed as soon as possible. In the meantime, you can put your own files into a folder named 'certs' in the root folder of the project. There you have to provide three files: cacert.pem, sdccert.pem, and userkey.pem

# Medical Device Description 
## General Description
The APS will be started with a description file of the medical device that shall be simulated. This description file has to contain exactly one msg:GetMdDescriptionResponse element that contains the description of the medical device. It shall be conformant with IEEE 11073-10207 (BICEPS). 

There are examples given in the folder 'example_descriptions/Descriptions'. The corresponding XSD schema files are given in the folder 'example_descriptions/Schemata'. 

## Simulator Behavior
The behavior of the simulator can be configured using extensions in the description file. The available extensions are defined and documented in 'example_descriptions/Schemata/APS_Behavior_Description_Extensions.xsd'. 

Make sure that the XML description file validates against the BICEPS XSDs and the APS XSD. 

### Numeric Metric Behavior 
Currently implemented numeric metric behaviors:

* monotonic increment/decrement of the metric value
* sampling the metric value from an equal distribution

See XSD file for documentation.
### Other Behavior Aspects
To be done...
